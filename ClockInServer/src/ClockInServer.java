import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.sun.net.httpserver.HttpServer;

import recursos.ArduinoManager;
import recursos.AutManager;
import recursos.MessageManager;
import recursos.UserManager;
import utils.Queries;

public class ClockInServer {
	
	private static final String url = "jdbc:mysql://localhost:3306/clockin";
	private static final String username = "root";
	private static final String password = "password";
	private static final String addr = "224.1.1.1" ;
	private static final int port = 8080;
	
	private static final int skt_port = 9000;
	private static final String hostname = "localhost";
	
	private static Connection con;
	private static Queries db;

	public static void main(String[] args){
		
		System.out.println("Connecting database...");
		try {
			con = DriverManager.getConnection(url, username, password);
			db = new Queries(con);
			System.out.println("\nConnected to the database!");
			
			InetAddress address = InetAddress.getByName(addr);
			
			URI baseUri = UriBuilder.fromUri("http://0.0.0.0/"+"ClockIn").port(port).build();
			ResourceConfig config = new ResourceConfig();
			
			UserManager 	uM 	= new UserManager(db);
			AutManager 		auM = new AutManager(db);
			ArduinoManager 	arM = new ArduinoManager(db);
			MessageManager 	mM 	= new MessageManager(db);
			
			config.register(uM).register(auM).register(arM).register(mM);
			
			HttpServer server = JdkHttpServerFactory.createHttpServer(baseUri, config);
			
			//InetSocketAddress skt = new InetSocketAddress(hostname, skt_port);
			//System.out.println(server.getAddress().getPort());
			//System.out.println(server.getAddress().getPo);
		
			//TODO might not be needed
//			new Thread( ()->{
//				
//				try {
//					ServiceAnnouncement sa = new ServiceAnnouncement(address);
//					sa.findClients();
//					
//				} catch (IOException e) {
//					System.err.println("Error related to sockets.");
//					e.printStackTrace();
//					System.exit(1);
//					
//				} 
//				
//			}).start();
			
			System.out.println("\nServer is now fully initiated!");
			
		} catch (SQLException e) {
			System.err.println("\nFailed to connect to the database!");
			e.printStackTrace();
			
		} catch (UnknownHostException e) {
			System.err.println("\nFailed to obtain InetAddress!");
			e.printStackTrace();
		}/* catch (IOException e) {
			System.err.println("Couldn�t bind to socket");
			e.printStackTrace();
		} */
	}

}
