package recursos;

import utils.ArduinoResponse;
import utils.Queries;

import javax.ws.rs.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import entidades.Entry;

@Path("/user")
public class UserManager {
	
	final static long CONFIRMATION_WINDOW = 10; //10 minutos
	final static long MILLIS_TO_MIN = 60000;
	final static int CONFIRMED = 1;
	
	final static String ERROR1 = "Error parsing JSON upon clockin log request.";
	final static String ERROR2 = "Error parsing response JSON upon clockin log request.";
	final static String ERROR3 = "Error parsing JSON upon confirmation request.";
	final static String ERROR4 = "Error processing user get picture request.";
	final static String ERROR5 = "Error processing user update request";
	
	final static int SUCCESS_C = 0;
	final static int ERROR_C1= 1;
	final static int ERROR_C2= 2;

	final static String JSON_MSG_SUCC = "Registo confirmado"; 
	final static String JSON_MSG_ERR1 = "Sem registos para confirmar"; 
	final static String JSON_MSG_ERR2 = "Prazo de confirmacao expirado";
	final static String JSON_MSG_ERR3 = "Erro ao confirmar registo"; 
	
	final static String DATE_FORMAT = "%d-%d-%d";
	final static String TIME_FORMAT = "%dh%d";
	
	final static String srvDir = "pics";
	final static String picFormat = "%s.png";
	final static String tsFormat = "%s %s";
	final static File basePath = FileSystems.getDefault().getPath(srvDir).toFile();
	
	Queries db;
	ArduinoResponse ar_msg;
	
	public UserManager(Queries db) {
		this.db = db;
		this.ar_msg = new ArduinoResponse();
	}
	
	@Path("/online")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsersOnline(){
		return Response.ok(this.db.selectUsersOnline()).build();
	}
	
	
	/*@Path("/pct/{pctName}")
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getUserPicture(@PathParam("pctName") String name){
		String filename = String.format(picFormat, name);
		File pic = new File(basePath,filename);
		byte[] b = new byte[65536];
		
		if(pic.exists()){
			try {
				b = Files.readAllBytes(pic.toPath());
				
			} catch (IOException e) {
				this.log(ERROR4);
				e.printStackTrace();
			}
		}
		
		return Response.ok(b).build();
		
	}*/
	
	@Path("/{id}/update/nm/{name}")
	@POST
	public Response setUserName(@PathParam("id") int id, @PathParam("name") String name){
			
		if(this.db.alterUserName(id, name)){
			return Response.ok().build();
		}else return Response.status(500).build();
		
	}
	
	@Path("/{id}/update/nr/{nr}")
	@POST
	public Response setUserPNumber(@PathParam("id") int id, @PathParam("nr") int nr){
			
		if(this.db.alterUserPhone(id, nr)){
			return Response.ok().build();
		}else return Response.status(500).build();
		
	}
	
	@Path("/{id}/cks")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//localhost:8080/ClockIn/user/7/cks?beginD=16-5-17&beginT=00h00&endD=16-5-17&endT=23h59
	public Response getUserClockIns(@PathParam("id") int idU, @QueryParam(value = "beginD") String b_d,
															  @QueryParam(value = "beginT") String b_t,
															  @QueryParam(value = "endD") String e_d,
															  @QueryParam(value = "endT") String e_t){
		
		String b_timestamp		= String.format(tsFormat, b_d, b_t);
		String e_timestamp		= String.format(tsFormat, e_d, e_t);
		
		System.out.println(b_timestamp);
		System.out.println(e_timestamp);
		
		return Response.ok(this.db.selectPicagemBetweenDatesByUserId(idU, b_timestamp, e_timestamp)).build();
		
		
	}

	@Path("/{id}/cks")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createClockIn(@PathParam("id") int id, String requestBody){
		
		try {
			this.log("here");
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(requestBody);
			this.log("here2");
			
			String timestamp 		= (String) res.get("timestamp");
			long status				= (Long) res.get("status");
			long type				= (Long) res.get("type");
		
			if(this.db.createPicagem(timestamp, type, status, id)){
				return Response.ok().build();
			}else return Response.status(404).build();
		
		} catch (Exception e) {
			this.log(ERROR3);
			e.printStackTrace();
			return Response.status(400).build();
		}	
		
	}
	
	/*@Path("{id}/cks/confirm")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response confirmClockIn(@PathParam("id") int id, String requestBody){
		
		try {
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(requestBody);
			
			String conf_timestamp = (String) res.get("timestamp");
		
			//obter os registos de entrada por confirmar do dia
			List<Entry> logs = this.db.selectPicagemEntradaUnconfirmed(id);
			
			if(!logs.isEmpty()){
				
				for( Entry e : logs){//averiguar qual dos registos est� dentro do intervalo v�lido para confirma��o
					
					if(inWindow(e.getTimeStamp(), conf_timestamp)){//encontrado registo
						if(this.db.alterPicagemStatus(e.getId(), CONFIRMED)){ //confirmar
							return Response.ok(this.buildArduinoMessage(SUCCESS, JSON_MSG_SUCC)).build();
							
						}else return Response.ok(this.buildArduinoMessage(ERROR, JSON_MSG_ERR3)).build();
					}
				}
				
				//os registos por confirmar ja estao fora do prazo de validade
				return Response.ok(this.buildArduinoMessage(ERROR, JSON_MSG_ERR2)).build();
							
			}else return Response.ok(this.buildArduinoMessage(ERROR, JSON_MSG_ERR1)).build();
		
		} catch (Exception e) {
			this.log(ERROR3);
			e.printStackTrace();
			return Response.status(400).build();
		}
	}*/
	
	@Path("{id}/cks/confirm")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response confirmClockIn(@PathParam("id") int id){
		
		String conf_timestamp = this.getCurrentTimestamp();
	
		//obter os registos de entrada por confirmar do dia
		List<Entry> logs = this.db.selectPicagemEntradaUnconfirmed(id);
		
		if(!logs.isEmpty()){
			
			for( Entry e : logs){//averiguar qual dos registos est� dentro do intervalo v�lido para confirma��o
				
				if(inWindow(e.getTimeStamp(), conf_timestamp)){//encontrado registo
					if(this.db.alterPicagemStatus(e.getId(), CONFIRMED)){ //confirmar
						return Response.ok(SUCCESS_C).build();
						
					}else return Response.ok(ERROR_C1).build();
				}
			}
			
			//os registos por confirmar ja estao fora do prazo de validade
			return Response.ok(ERROR_C2).build();
						
		}else return Response.ok(ERROR_C1).build();
	
		
	}


	
	private boolean inWindow(String e_tms, String conf_tms){
		GregorianCalendar e_c = parseDate(e_tms);
		GregorianCalendar r_c = parseDate(conf_tms);
		
		long timelapse = (r_c.getTimeInMillis() - e_c.getTimeInMillis()) / MILLIS_TO_MIN;
		
		if(timelapse<=CONFIRMATION_WINDOW){
			return true;
		} else return false;
		

	}
	
	private GregorianCalendar parseDate(String timestamp){
		String[] datetime = timestamp.split(" ");
		String [] date = datetime[0].split("-");
		String [] time = datetime[1].split("h");
		
		int year,month,day,hour,minute;
		
		year 	= Integer.parseInt(date[2]);
		month	= Integer.parseInt(date[1]);
		day 	= Integer.parseInt(date[0]);
		
		hour		= Integer.parseInt(time[0]);
		minute		= Integer.parseInt(time[1]);
		
		return new GregorianCalendar(year,month-1,day,hour,minute);
	}
	
	private String getCurrentTimestamp(){
		GregorianCalendar c = new GregorianCalendar();
		
		String date = String.format(DATE_FORMAT, c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.MONTH)+1, c.get(Calendar.YEAR));
		String time = String.format(TIME_FORMAT, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));
		
		String final_tms = date +" "+time;
		return final_tms;
	}
	
	private ArduinoResponse buildArduinoMessage(int code, String msg){
		this.ar_msg.setCode(code);	
		this.ar_msg.setMsg(msg);
		
		return this.ar_msg;
	}
	
	
	private void log(String msg) {
		System.err.println("\n");
		System.err.println(">>>>>>>>>>>UserManager:");
		System.err.println(msg);
	}

}
