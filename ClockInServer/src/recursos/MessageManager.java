package recursos;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import utils.Queries;

@Path("/message")
public class MessageManager {

	static final String JSON_ERROR = "Error parsing JSON upon create message request.";
	Queries db;

	public MessageManager( Queries db) {
		this.db = db;
	}
	
	@POST
	@Path("/new")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createMeassage(String requestBody){
		try {
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(requestBody);
			
			long idFrom			= (Long) res.get("idFrom");
			long idTo			= (Long) res.get("idTo");
			String text 		= (String) res.get("text");
			String timestamp 	= (String) res.get("timestamp");
			
			if(this.db.createMessage(text, timestamp, idTo, idFrom)){
				return Response.ok().build();
			} else return Response.status(500).build();
			
		} catch (Exception e) {
			this.log(JSON_ERROR);
			e.printStackTrace();
			return Response.status(400).build();
		}
		
	}
	
	@GET
	@Path("/{idFrom}/conv/{idTo}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getExchangedMessages(@PathParam("idFrom") int idFrom, @PathParam("idTo") int idTo){
		return Response.ok(this.db.selectExchangedMesasges(idFrom,idTo)).build();
	}
	
	private void log(String msg) {
		System.err.println("\n");
		System.err.println(">>>>>>>>>>>MessageManager:");
		System.err.println(msg);
		
	}

}
