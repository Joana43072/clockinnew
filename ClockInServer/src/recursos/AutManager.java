package recursos;

import utils.Queries;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import entidades.User;



@Path("/auth")
public class AutManager {
	
	Queries db;
	
	static final String UNAUTHORIZED = "User Authentication Failed"; 
	static final String JSON_ERROR = "Error parsing JSON upon signin/signoff request.";
	static final int ONLINE = 1;
	static final int OFFLINE = 0;
	
	public AutManager( Queries db) {
		this.db = db;
	}
	
	@POST
	@Path("/signin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login (String requestBody) {
		
		try {
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(requestBody);
			
			String un = (String) res.get("username");
			String pwd = (String) res.get("password");
			
			User u = db.selectUserByCredentials(un,pwd);
			
			if(u!=null){
				return db.alterUserOnline(u.getId(),ONLINE) ? Response.ok(u).build() : Response.ok(400).build();
			} else{
				this.log(UNAUTHORIZED);
				return Response.status(401).build();
			}
			
		} catch (ParseException e) {
			this.log(JSON_ERROR);
			return Response.ok(400).build();
		}
	
	}
	
	@POST
	@Path("/signoff")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response logoff(String requestBody) {
				
		try {
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(requestBody);
			
			String id = (String) res.get("id");
			
			if(db.alterUserOnline(Integer.parseInt(id),OFFLINE)){
				return Response.ok().build();
			}else return Response.status(404).build();
			
		} catch (ParseException e) {
			this.log(JSON_ERROR);
			return Response.ok(400).build();
		}
	}
	
	private void log(String msg){
		System.err.println("\n");
		System.err.println(">>>>>>>>>>>AuthManager:");
		System.err.println(msg);
	}
	
	

}
