package recursos;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import entidades.ArduinoController;
import utils.Queries;


@Path("/arduino")
public class ArduinoManager {
	
	static final int ONLINE 	= 1;
	static final int OFFLINE 	= 0;
	static final String JSON_ERROR = "Error parsing JSON upon registering request.";

	private Queries db;

	public ArduinoManager( Queries db) {
		this.db = db;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getArduino(@PathParam("id") int id){
		ArduinoController a = this.db.selectArduinoById(id);
		return a!=null ? Response.ok(a).build() : Response.status(400).build();
		
	}
	
	@GET
	@Path("/all/active")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listArduinos(){
		return Response.ok(this.db.selectActiveArduinos()).build();
	}
	
	@POST
	@Path("/{id}/register")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerArduino(@PathParam("id") int id, String requestBody){ //TODO da erro
		
		try {
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(requestBody);
			
			String addr = (String) res.get("ipAddress");
			String port = (String) res.get("port");
			
			if(this.db.alterArduinoIpPort(id, addr, port)){//register address and port
				if(this.db.alterArduinoOnline(id, ONLINE)){///set arduino online
					return Response.ok().build();
					
				}else return Response.status(500).build();
				
			}else return Response.status(404).build();
			
		} catch (ParseException e) {
			this.log(JSON_ERROR);
			return Response.ok(400).build();
		}
			
	}

	private void log(String msg) {
		System.err.println("\n");
		System.err.println(">>>>>>>>>>>ArduinoManager:");
		System.err.println(msg);
		
	}

}
