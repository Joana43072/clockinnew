import java.io.*;
import java.net.*;

public class ServiceAnnouncement {

	final static int PORT = 9000;
	final static int SIZE = 512;
	final static long SLEEP = 15000;
	final static String SRV_HELLO = "http://192.168.1.2:8080/ClockIn"; //TODO update this everytime
	
	InetAddress multicastAddress;
	
	
	public ServiceAnnouncement(InetAddress ia) {
		this.multicastAddress = ia;
	}
	
	public void findClients () throws IOException{
		
		MulticastSocket skt = new MulticastSocket();
		
		while(true){
			byte[] buff0 = SRV_HELLO.getBytes();
			DatagramPacket anPkt = new DatagramPacket(buff0, buff0.length);
			anPkt.setAddress(multicastAddress);
			anPkt.setPort(PORT);
			skt.send(anPkt);
				
			try {
				Thread.sleep(SLEEP);
			} catch (InterruptedException e) {
				this.log(e.getMessage());
			}
			
		}
		
	}
	
	public void log(String msg){
		System.err.println("\n");
		System.err.println(">>>>>>>>>>>Service Announcement:");
		System.err.println(msg);
	}

}
