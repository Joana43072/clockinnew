package entidades;

public interface ArduinoController {

	int getId();
	String getIPAddres();
	String getPort();
	int isOnline();
}
