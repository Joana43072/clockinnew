package entidades;

public class MessageClass implements Message {

	private String timestamp, text;
	private int idFrom, idTo, id;

	public MessageClass(int id, int idTo, int idFrom, String text, String timestamp ) {
		this.id 		= id;
		this.idTo 		= idTo;
		this.idFrom 	= idFrom;
		this.text		= text;
		this.timestamp	= timestamp;
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public String getTimeStamp() {
		return this.timestamp;
	}

	@Override
	public int getFrom() {
		return this.idFrom;
	}

	@Override
	public int getTo() {
		return this.idTo;
	}

}
