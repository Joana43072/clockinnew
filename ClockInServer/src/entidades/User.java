package entidades;

public interface User {
	
	int getId();
	
	String getUsername();
	
	String getPassword();
	
	int getType();
	
	int isOnline();
	
	String getName();
	
	int getPhoneNumber();
	
	String getEmail();
	
	String getPost();
	
	int getArduino();
	
	String getOffice();

}
