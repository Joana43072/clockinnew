package entidades;

public interface Entry {
	
	int getId();
	
	String getTimeStamp();
	
	int getType();
	
	int getUser();
	
	int getStatus();

}
