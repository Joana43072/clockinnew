package entidades;

public class UserClass implements User{ //TODO acrescentar settings
	
	int online;
	
	int phone;
	
	int arduino; //id, type,
	
	String post;
	
	String name;
	
	String email; //username, password
	int id;
	
	int type;
	
	String username;
	
	String password;
	
	String pictureAddress;
	
	String office;
	
	public UserClass(int id, String username, String password, int type,  int online,
						   String name, String email, String post, int nr, int idA, String office){
		this.id=id;
		this.username=username;
		this.password=password;
		this.type = type;
		
		this.name=name;
		this.email=email;
		this.phone = nr;
		this.post=post;
		this.arduino =idA;
		this.online=online;
		this.office = office;

	}
	
	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public int getType() {
		return this.type;
	}

	@Override
	public int isOnline() {
		return this.online;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int getPhoneNumber() {
		return this.phone;
	}

	@Override
	public String getPost() {
		return this.post;
	}

	@Override
	public int getArduino() {
		return this.arduino;
	}

	@Override
	public String getEmail() {
		return this.email;
	}

	@Override
	public String getOffice() {
		return this.office;
	}

}
