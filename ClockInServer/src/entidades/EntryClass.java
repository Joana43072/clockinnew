package entidades;

public class EntryClass implements Entry {

	 String timestamp;
	 int id, type, status, idUser; //false-> nao confirmado, true->confirmado

	public EntryClass(int id, String timestamp, int type, int idUser, int status) {
		this.id = id;
		this.timestamp	= timestamp;
		this.type 	= type;
		this.idUser	= idUser;
		this.status = status;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public String getTimeStamp() {
		return this.timestamp;
	}

	@Override
	public int getType() {
		return this.type;
	}

	@Override
	public int getUser() {
		return this.idUser;
	}

	@Override
	public int getStatus() {
		return this.status;
	}

	

}
