package entidades;

public class ArduinoControllerClass implements ArduinoController{

	String port;
	String ip;
	int id, isOnline;

	public ArduinoControllerClass(int id, String ip, String port, int isOnline) {
		this.id 		= id;
		this.ip 		= ip;
		this.port 		= port;
		this.isOnline 	= isOnline;
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String getIPAddres() {
		return this.ip;
	}

	@Override
	public String getPort() {
		return this.port;
	}

	@Override
	public int isOnline() {
		return isOnline;
	}

}
