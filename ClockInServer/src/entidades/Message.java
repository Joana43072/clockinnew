package entidades;

public interface Message {
	int getId();
	
	String getText();
	
	String getTimeStamp();
	
	int getFrom();
	
	int getTo();

}
