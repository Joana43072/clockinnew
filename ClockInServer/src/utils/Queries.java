package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entidades.ArduinoController;
import entidades.Entry;
import entidades.Message;
import entidades.User;
import entidades.UserClass;

public class Queries {
	
	public static final String ERROR_LOG = "An error occured with query: %s. \nError: %s.";
	
	public static final String INSERT = "INSERT INTO";
	public static final String VALUES = "VALUES";
	public static final String SELECT = "SELECT";
	public static final String FROM = "FROM";
	public static final String ALL = "*";
	public static final String WHERE = "WHERE";
	public static final String AND = "AND";
	public static final String UPDATE = "UPDATE";
	public static final String SET = "SET";
	public static final String OR = "OR";
	
	/*Table arduino and its columns*/
	public static final String TABLE_ARDUINO = "arduino";
	public static final String TABLE_ARDUINO_COL_1 = "idarduino";
	public static final String TABLE_ARDUINO_COL_2 = "ip_address";
	public static final String TABLE_ARDUINO_COL_3 = "port";
	public static final String TABLE_ARDUINO_COL_4 = "online";
	
	/*Table user and its columns*/
	public static final String TABLE_USER = "user";
	public static final String TABLE_USER_COL_1 = "iduser";
	public static final String TABLE_USER_COL_2 = "online";
	public static final String TABLE_USER_COL_3 = "username";
	public static final String TABLE_USER_COL_4 = "password";
	public static final String TABLE_USER_COL_5 = "name";
	public static final String TABLE_USER_COL_6 = "phone_nr";
	public static final String TABLE_USER_COL_7 = "post";
	public static final String TABLE_USER_COL_8 = "arduino_idarduino";
	public static final String TABLE_USER_COL_9 = "type";
	public static final String TABLE_USER_COL_10 = "email";
	public static final String TABLE_USER_COL_11 = "photo";
	public static final String TABLE_USER_COL_12 = "office";
	public static final int ONLINE = 1;
	public static final int OFFLINE = 0;
	
	/*Table picagem and its columns*/
	public static final String TABLE_PICAGEM = "picagem";
	public static final String TABLE_PICAGEM_COL_1 = "idregist";
	public static final String TABLE_PICAGEM_COL_2 = "timestamp";
	public static final String TABLE_PICAGEM_COL_3 = "type";
	public static final String TABLE_PICAGEM_COL_4 = "status";
	public static final String TABLE_PICAGEM_COL_5 = "user_iduser";
	public static final int UNCONFIRMED = 0;
	public static final int ENTRADA = 0;
	
	/*Table message and its columns*/
	public static final String TABLE_MESSAGE = "message";
	public static final String TABLE_MESSAGE_COL_1 = "idMessage";
	public static final String TABLE_MESSAGE_COL_2 = "text";
	public static final String TABLE_MESSAGE_COL_3 = "timestamp";
	public static final String TABLE_MESSAGE_COL_4 = "user_iduserTo";
	public static final String TABLE_MESSAGE_COL_5 = "user_iduserFrom";
	
	
	Connection con;
	DBParser parser;
	
	public Queries(Connection con){
		this.con=con;
		this.parser = new DBParser();
	}
	
	/***************************************************************/
	/***************************************************************/
	/************************** ARDUINO ****************************/
	/***************************************************************/
	/***************************************************************/
		
	public List<ArduinoController> selectActiveArduinos(){
		String query = SELECT+" "+ALL+" "+FROM+" "+TABLE_ARDUINO+" "+WHERE+" "+TABLE_ARDUINO_COL_4+" ='"+1+"'";
		log(query);
		
		List<ArduinoController> aList = new ArrayList<ArduinoController>(20);
		
		try{
			
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
			
			
			while(result.next()){
				aList.add(this.parser.parseArduino(result));
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+")";
				System.err.println("Arduino: "+obj);
			}
			
			return aList;
			
		}catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return aList;
		}
	}
	
	public ArduinoController selectArduinoById( final int id){
		String query = SELECT+" "+ALL+" "+FROM+" "+TABLE_ARDUINO+" "+WHERE+" "+TABLE_ARDUINO_COL_1+"='"+id+"'";
		log(query);
		ArduinoController a = null;
		
		try{
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
			
			while(result.next()){
				a = this.parser.parseArduino(result);
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+")";
				System.err.println("Result: "+obj);
			}
		
			return a;
		
		}catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return a;
		}
	}

	public boolean alterArduinoOnline(int id, int v){
		
		String query = UPDATE+" "+TABLE_ARDUINO+" "+SET+" "+TABLE_ARDUINO_COL_4+"='"+v+"' "+WHERE+" "+TABLE_ARDUINO_COL_1+"='"+id+"'";
		this.log(query);
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			int r = statement.executeUpdate();
			System.err.println(r);
			
			return r==1 ? true: false;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
		
	}

	public boolean alterArduinoIpPort( int id, String ip_address, String port){
		String query = UPDATE+" "+TABLE_ARDUINO+" "+SET+" "+TABLE_ARDUINO_COL_2+"='"+ip_address+"', "+TABLE_ARDUINO_COL_3+"='"+port+"' "+WHERE+" "+TABLE_ARDUINO_COL_1+"='"+id+"'";
		this.log(query);
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			int r = statement.executeUpdate();
			System.err.println(r);
			
			return r==1 ? true: false;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
	}
	
	/***************************************************************/
	/***************************************************************/
	/*************************** USER ******************************/
	/***************************************************************/
	/***************************************************************/
	
	
 /*	public void createUser(final int online, final String username, final String password, final String name, final int phone_nr, final String post, final int arduino_idarduino, final int type) throws SQLException{
		String query = INSERT+" "+TABLE_USER+" ("+TABLE_USER_COL_2+","+TABLE_USER_COL_3+","+TABLE_USER_COL_4+","+
				TABLE_USER_COL_5+","+TABLE_USER_COL_6+","+TABLE_USER_COL_7+","+TABLE_USER_COL_8+","+TABLE_USER_COL_9+") "+
				VALUES+" ('"+online+"','"+username+"','"+password+"','"+name+"','"+phone_nr+"','"+post+"','"+
				arduino_idarduino+"','"+type+"')";
		con.prepareStatement(query).executeUpdate();
	}*/
		
	public User selectUserById(int id){
		String query = SELECT+" "+ALL+" "+FROM+" "+TABLE_USER+" "+WHERE+" "+TABLE_USER_COL_1+"='"+id+"'";
		this.log(query);
		
		User u = null;
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
			
			while(result.next()){			
				u = this.parser.parseUser(result);
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+", "+result.getString(5)+", "+result.getString(6)+", "+result.getString(7)+", "+result.getString(8)+", "+result.getString(9)+", "+result.getString(10)+", "+result.getString(11)+")";
				System.err.println("Result: "+obj);
			}
			
			return u;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return u;
		}
			
	}
	
	public User selectUserByCredentials(String un, String pwd){
		String query = SELECT+" "+ALL+" "+FROM+" "+TABLE_USER+" "+WHERE+" "+TABLE_USER_COL_3+"='"+un+"' "+
				AND+" "+TABLE_USER_COL_4+"='"+pwd+"'";
		this.log(query);
		
		User u = null;
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
			
			while(result.next()){			
				u = this.parser.parseUser(result);
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+", "+result.getString(5)+", "+result.getString(6)+", "+result.getString(7)+", "+result.getString(8)+", "+result.getString(9)+", "+result.getString(10)+", "+result.getString(11)+")";
				System.err.println("Result: "+obj);
			}
			
			return u;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return u;
		}
			
		
		
	}
	
	public List<User> selectUsersOnline(){
		String query = SELECT+" "+ALL+" "+FROM+" "+TABLE_USER+" "+WHERE+" "+TABLE_USER_COL_2+"='"+ONLINE+"'";
		this.log(query);
		
		List<User> users = new ArrayList<>();
		
		try{
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
			
			while(result.next()){
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+", "+result.getString(5)+", "+result.getString(6)+", "+result.getString(7)+", "+result.getString(8)+", "+result.getString(9)+", "+result.getString(10)+", "+result.getString(11)+")";
				System.out.println("Result: "+obj);
				users.add(this.parser.parseUser(result));
			}
			
			return users;
		}catch(SQLException e){
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return users;
		}
	}

	public ArduinoController selectUserArduino( int id){
		User u = this.selectUserById(id);
		if(u!=null){
			return this.selectArduinoById(u.getArduino());
			
		}else return null;
		
	}
	
	public boolean alterUserOnline(int id, int v){
		String query = UPDATE+" "+TABLE_USER+" "+SET+" "+TABLE_USER_COL_2+"='"+v+"' "+WHERE+" "+TABLE_USER_COL_1+"='"+id+"'";
		this.log(query);
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			int r = statement.executeUpdate();
			System.err.println(r);
			
			return r==1 ? true: false;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
			
	}

	public boolean alterUserName(int id, String v){
		String query = UPDATE+" "+TABLE_USER+" "+SET+" "+TABLE_USER_COL_5+"='"+v+"' "+WHERE+" "+TABLE_USER_COL_1+"='"+id+"'";
		this.log(query);
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			int r = statement.executeUpdate();
			System.err.println(r);
			
			return r==1 ? true: false;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
			
	}
	
	public boolean alterUserPhone(int id, int v){
		String query = UPDATE+" "+TABLE_USER+" "+SET+" "+TABLE_USER_COL_6+"='"+v+"' "+WHERE+" "+TABLE_USER_COL_1+"='"+id+"'";
		this.log(query);
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			int r = statement.executeUpdate();
			System.err.println(r);
			
			return r==1 ? true: false;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
			
	}
			
	public boolean alterUserPhoto(int id, String v){
		String query = UPDATE+" "+TABLE_USER+" "+SET+" "+TABLE_USER_COL_11+"='"+v+"' "+WHERE+" "+TABLE_USER_COL_1+"='"+id+"'";
		this.log(query);
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			int r = statement.executeUpdate();
			System.err.println(r);
			
			return r==1 ? true: false;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
			
	}
	
	/***************************************************************/
	/***************************************************************/
	/************************** PICAGEM ****************************/
	/***************************************************************/
	/***************************************************************/
	
	
	public boolean createPicagem(String timestamp, long type, long status, int user_iduser){
		String query = INSERT+" "+TABLE_PICAGEM+" ("+TABLE_PICAGEM_COL_2+","+TABLE_PICAGEM_COL_3+","+TABLE_PICAGEM_COL_4+","+TABLE_PICAGEM_COL_5+") "+
				VALUES+" ('"+timestamp+"','"+type+"','"+status+"','"+user_iduser+"')";
		this.log(query);
		
		try{
			int r = con.prepareStatement(query).executeUpdate();
			System.err.println("Result: "+r);
			return true;
		}catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
		
	}
	
	public ResultSet selectAllPicagens() throws SQLException{
		String query = SELECT+" "+ALL+" "+FROM+" "+TABLE_PICAGEM;
		PreparedStatement statement = con.prepareStatement(query);
		ResultSet result = statement.executeQuery();
		
		System.out.println(">>>>>>>>>>>DB QUERY: "+query);
		
		while(result.next()){
			String obj = result.getString(1)+" "+result.getString(2)+" "+result.getString(3)+" "+result.getString(4)+" "+result.getString(5);
			System.out.println(obj);
		}
		System.out.println("<<<<<<<<<<<");
		
		return result;
	}
	
	//TODO esta errado e nao e preciso, usa-se o debaixo com os parametros apropriados
	/*public List<Entry> selectPicagemByDateByUserId( int id, String date) {
		String query =   SELECT+" "+ALL+" "+FROM+" "+TABLE_PICAGEM+" "+WHERE+" "+TABLE_PICAGEM_COL_5+"='"+id+"' AND "+TABLE_PICAGEM_COL_2+"='"+date+"' ORDER BY "+TABLE_PICAGEM_COL_2;
		this.log(query);
		
		List<Entry> log = new ArrayList<>();
		
		try{
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
					
			while(result.next()){
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+", "+result.getString(5)+")";
				System.err.println("Result: "+obj);
				log.add(this.parser.parseEntry(result));
			}
			
			return log;
			
		}catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return log;
		}		
		
	}*/
	
	public List<Entry> selectPicagemBetweenDatesByUserId( int id, String b_ts, String e_ts) {
		String query =   SELECT+" "+ALL+" "+FROM+" "+TABLE_PICAGEM+" "+WHERE+" "+TABLE_PICAGEM_COL_5+"='"+id+"' AND "+TABLE_PICAGEM_COL_2+" BETWEEN '"+b_ts+"' AND '"+e_ts+"' ORDER BY "+TABLE_PICAGEM_COL_2+" DESC";
		this.log(query);
		
		List<Entry> log = new ArrayList<>();
		
		try{
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
					
			while(result.next()){
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+", "+result.getString(5)+")";
				System.err.println("Result: "+obj);
				log.add(this.parser.parseEntry(result));
			}
			
			return log;
			
		}catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return log;
		}		
		
	}

	public boolean alterPicagemStatus(int id, int status){
		String query = UPDATE+" "+TABLE_PICAGEM+" "+SET+" "+TABLE_PICAGEM_COL_4+"='"+status+"' "+WHERE+" "+TABLE_PICAGEM_COL_1+"='"+id+"'";
		this.log(query);
		
		try {
			PreparedStatement statement = con.prepareStatement(query);
			int r = statement.executeUpdate();
			System.err.println(r);
			
			return r==1 ? true: false;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
	}
	
	public List<Entry> selectPicagemEntradaUnconfirmed(int idU){
		String query = SELECT+" "+ALL+" "+FROM+" "+TABLE_PICAGEM+" "+WHERE+" "+TABLE_PICAGEM_COL_5+" ='"+idU+"' "+AND+" "+TABLE_PICAGEM_COL_4+" ='"+UNCONFIRMED+"' "+AND+" "+TABLE_PICAGEM_COL_3+" ='"+ENTRADA+"'";
		this.log(query);
		
		List<Entry> log = new ArrayList<>();
		
		try{
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
					
			while(result.next()){
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+", "+result.getString(5)+")";
				System.err.println("Result: "+obj);
				log.add(this.parser.parseEntry(result));
			}
			
			return log;
			
		}catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return log;
		}		
	}
	
	/***************************************************************/
	/***************************************************************/
	/************************** MESSAGE ****************************/
	/***************************************************************/
	/***************************************************************/
	
 	public boolean createMessage( String text, String timestamp, long user_iduserTo, long user_iduserFrom){
		String query = INSERT+" "+TABLE_MESSAGE+" ("+TABLE_MESSAGE_COL_2+","+TABLE_MESSAGE_COL_3+","+TABLE_MESSAGE_COL_4+","+TABLE_MESSAGE_COL_5+") "+
				VALUES+" ('"+text+"','"+timestamp+"','"+user_iduserTo+"','"+user_iduserFrom+"')";
		this.log(query);
		
		try{
			int r = con.prepareStatement(query).executeUpdate();
			System.err.println("Result: "+r);
			return true;
			
		} catch (SQLException e) {
			log(String.format(ERROR_LOG, query, e.getMessage()));
			e.printStackTrace();
			return false;
		}
		
	}
	
	public List<Message> selectExchangedMesasges(int id1, int id2){
		String query = SELECT+" "+ALL+" "+FROM+" "+TABLE_MESSAGE+" "+WHERE+" "+"("+TABLE_MESSAGE_COL_4+" = '"+id1+"' "+OR+" "+TABLE_MESSAGE_COL_5+" = '"+id1+"')"+" "+AND+" ("+TABLE_MESSAGE_COL_4+" = '"+id2+"' "+OR+" "+TABLE_MESSAGE_COL_5+" = '"+id2+"')";
		this.log(query);
		List<Message> msgs = new ArrayList<>();
		
		try{
			PreparedStatement statement = con.prepareStatement(query);
			ResultSet result = statement.executeQuery();
			
			while(result.next()){
				String obj = "("+result.getString(1)+", "+result.getString(2)+", "+result.getString(3)+", "+result.getString(4)+", "+result.getString(5)+")";
				System.err.println("Result: "+obj);
				msgs.add( this.parser.parseMessage(result));
			}
			return msgs;
			
		}catch (SQLException e) {
				log(String.format(ERROR_LOG, query, e.getMessage()));
				e.printStackTrace();
				return msgs;
		}	
		
	}
	
	private void log(String msg){
		
		System.err.println("\n");
		System.err.println(">>>>>>>>>>>DB:");
		System.err.println(msg);
	}
}
