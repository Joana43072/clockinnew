package utils;

import java.sql.ResultSet;
import java.sql.SQLException;

import entidades.ArduinoController;
import entidades.ArduinoControllerClass;
import entidades.Entry;
import entidades.EntryClass;
import entidades.Message;
import entidades.MessageClass;
import entidades.User;
import entidades.UserClass;

public class DBParser {

	public DBParser() {}
	
	public User parseUser(ResultSet result) throws SQLException{
		
		int id			= result.getInt(1);
		int isOnline	= result.getInt(2);
		String un 		= result.getString(3);
		String pwd 		= result.getString(4);
		String name 	= result.getString(5);
		int nr 			= result.getInt(6);
		String post 	= result.getString(7);
		int idA 		= result.getInt(8);
		int type 		= result.getInt(9);
		String email 	= result.getString(10);
		String office 	= result.getString(11);
		
		return new UserClass(id,un,pwd,type,isOnline,name,email,post,nr,idA, office);
	}
	
	public ArduinoController parseArduino (ResultSet result) throws SQLException{
		
		int id 			= result.getInt(1);
		String address 	= result.getString(2);
		String port 	= result.getString(3);
		int isOnline 	= result.getInt(4);
		
		return new ArduinoControllerClass(id,address,port,isOnline);
		
	}
	
	public Message parseMessage (ResultSet result) throws SQLException {
		int id				= result.getInt(1);
		String text			= result.getString(2);
		String timestamp 	= result.getString(3);
		int id_to 			= result.getInt(4);
		int id_from 		= result.getInt(5);
		
		return new MessageClass(id,id_to, id_from, text, timestamp);
	}

	public Entry parseEntry(ResultSet result) throws SQLException {
		int id				= result.getInt(1);
		String timestamp	= result.getString(2);
		int type			= result.getInt(3);
		int status			= result.getInt(4);
		int idUser			= result.getInt(5);
		
		return new EntryClass(id,timestamp,type,idUser,status);
	}

}
