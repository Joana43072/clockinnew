package utils;

public class ArduinoResponse {
	
	int op_status_code;
	String op_status_msg;

	public ArduinoResponse() {}

	public void setCode(int code) {
		this.op_status_code = code;
	}
	
	public int getCode() {
		return this.op_status_code;
	}

	public void setMsg(String msg) {
		this.op_status_msg = msg;
	}
	
	public String getMsg() {
		return this.op_status_msg;
	}
	
	

}
