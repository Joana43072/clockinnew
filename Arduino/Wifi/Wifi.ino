#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <ArduinoHttpClient.h>

/*Ligar ao wifi*/
const char *ssid0       = "ClockInWeb";
const char *password0   = "clockinrules";

/*Criar AP*/
const char *ap_ssid     = "ClockInWeb";
const char *ap_password = "clockinrules";

IPAddress myIP;
IPAddress myAP_ID;

ESP8266WebServer server(80);


const String url = "/user/USERID/cks/confirm"; //CHANGE
const String srv_ip = "192.168.56.1"; //CHANGE
const int srv_port = 8080;
boolean pendingRequest = false;

void setup() {
  
  //serial para comunicar com arduino
  delay(1000);
  Serial.begin(115200);

 /*Quando queremos aceder a uma net ja existente */
  WiFi.begin(ssid0,password0);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    //Serial.print(".");
  }
  //Serial.println();

  //Serial.print("Connected, IP address: ");
  //myIP = WiFi.localIP();
  //Serial.println(myIP);

  //Serial.println();
  //Serial.print("Ip servidor");
  //Serial.println(ip_srv);


/*Quando vamos criar o nosso AP
*Comentar os prints qd formos comunicar com o arduino
*/
  /*
  //Serial.println("Configuring access point...");

  WiFi.softAP(ap_ssid, ap_password);
  myAP_ID = WiFi.softAPIP(); //ip com o qual vamos comunicar
  
  //Serial.print("AP IP address: ");
  //Serial.println(myAP_ID );
  */
  
  server.on("/meeting", handleMeeting);
  server.begin();
}

void loop() {  
  server.handleClient();

  if(!Serial.available()){
    return;
  }

  /*Pedido de confirmacao*/
  if (Serial.available() > 0) {
    
    char in = Serial.read();
    if(in=='C'){
      //Serial.println("Confirmation Button");
      processBtnRequest();
    }
  }

  delay(1000);
}

void processBtnRequest(){
  WiFiClient wifi;
  HttpClient client = HttpClient(wifi, srv_ip, srv_port);
  String contentType = "";
  String body = "";
  client.post(url, contentType, body);
  
}

void handleMeeting(){
  String data = server.arg("plain");
  StaticJsonBuffer<200> jBuffer;
  JsonObject& jObject = jBuffer.parseObject(data);
  String msg = jObject["office"];
  Serial.write('M');
  Serial.print(msg);
  Serial.println();
  server.send(204,"");
}
