#include <LiquidCrystal.h>
#include <SoftwareSerial.h>
#include <SharpIR.h>

//lcd
LiquidCrystal lcd(2,3, 4,5,7,8);
const int contraste = 70;
const int contrastePin = 6;

//botao
const int btnPin    = 13  ;
int btnStatus = 0;

//led
const int ledPin    = 12;

//buzzer
const int buzzerPin = 11;
int melody[] = {98, 104, 110, 110, 104, 98}; //notes in the melody
int noteDurations[] = {8, 8, 4, 4, 8, 8}; // note durations: 4 = quarter note, 8 = eighth note, etc.:

//infravermelhos
const int sharpIVPin     = A0;
SharpIR sharp(sharpIVPin, 1080);

// WIFI
const int RX = 0;
const int TX = 1;
SoftwareSerial ESPserial(RX,TX);
boolean pendingRequest = false;

//WIFI rsp msgs
const String meeting0 = "Convocado para a";
const String meeting1 = "sala 123";
const String confirmationMSG0 = "Presenca confirmada";
const String confirmationMSG1 = "Erro ao confirmar";
const String confirmationMSG20 = "Prazo de confirmacao";
const String confirmationMSG21 = "expirado";

void setup() {

  ESPserial.begin(115200); //wifi shield
  Serial.begin(9600); //consola

  //setup lcd
  lcd.begin(16, 2);
  pinMode(contrastePin, OUTPUT);
  analogWrite(contrastePin, contraste);
  lcd.print("Welcome!");

  //setup pins
  pinMode(btnPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(sharpIVPin, INPUT);

}

void loop() {  
 

  if(!ESPserial){
    Serial.println("Not available");
    return;
  }

  if(ESPserial.available() >0){
  char in = ESPserial.read();
 
  if (in=='C'){
    pendingRequest = false;

    char code = ESPserial.read();
    handleConfirmationRsp(code);

  }else if(in=='M'){

    //ler a sala
    String readCodes[20];
    char code = ESPserial.read();
    int i=0;
    while(code!='\n'){
      Serial.print("Code gotten: ");
      Serial.println(code);
      
      readCodes[i] = String(code);
      
      Serial.print("Room read so far: ");
      Serial.println(readCodes[i]);

      code = ESPserial.read();
      i++;
    }

    String room = "";
    for(int j=0; j<=i; j++){
      room = room + readCodes[j];
    }

    Serial.println(room);
    
    //print para o lcd
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(meeting0);
    lcd.setCursor(0,1);
    lcd.println(meeting1+room);

    processIV();
    
  }
  
 }

 
  btnStatus = digitalRead(btnPin); 
  if (btnStatus == HIGH){
    ESPserial.write('C');
    //Serial.println("Atempting to send btn request to wifi");
  
  }    
  
 delay(500);
}

void processIV(){

  int dist = sharp.distance();

  //Serial.print("Distancia: ");
  //Serial.println(dist);

  if(dist <= 35){//led
    ledblink();
    
  }else {//buzzer
    tone(buzzerPin, 110, 60000); //buzzer durante 1min
    //Serial.println("buzzer");
  }
  
}

void ledblink(){ 

  for(int t = 0; t<15; t++){//led will blink 15 times
     digitalWrite(ledPin, HIGH);
     delay(1000);  
     digitalWrite(ledPin, LOW);
     delay(1000);  
  }
  
  
}

void handleConfirmationRsp(char code){

  switch(code){
    case '0': lcd.print(confirmationMSG0);
    break;

    case '1': lcd.print(confirmationMSG1);;
    break;

    case '2':{ 
      lcd.print(confirmationMSG20);
      lcd.print(confirmationMSG21);
    };
    break;
  }
}
