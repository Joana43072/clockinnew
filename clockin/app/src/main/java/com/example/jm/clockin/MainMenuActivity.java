package com.example.jm.clockin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jm.clockin.Chat.ListUsers.ListUsersFragment;
import com.example.jm.clockin.Client.Tower;
import com.example.jm.clockin.Dialogs.LogoutDialog;
import com.example.jm.clockin.Fragments.HistoryFragment;
import com.example.jm.clockin.Fragments.ManualClockInFragment;
import com.example.jm.clockin.Fragments.MeetingsFragment;
import com.example.jm.clockin.Fragments.ProfileFragment;
import com.example.jm.clockin.Fragments.QuickMenuFragment;
import com.example.jm.clockin.Fragments.SettingsFragment;
import com.example.jm.clockin.Models.UsersModel;

public class MainMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private QuickMenuFragment quickMenuFragment;
    private HistoryFragment historyFragment;
    private MeetingsFragment meetingsFragment;
    private ListUsersFragment listUsersFragment;
    private SettingsFragment settingsFragment;
    private ProfileFragment profileFragment;
    private ManualClockInFragment manualClockInFragment;
    private FragmentManager fragmentManager;

    private Toolbar toolbar;
    private ImageView user_photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main_menu);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        UsersModel u = ((Tower) getApplicationContext()).getUserLoggedIn();
        if(u==null){
            this.logout();
            Toast.makeText(getApplicationContext(), "A sua sessão expirou, efetue login novamente", Toast.LENGTH_LONG).show();
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //hide menu items according to user type and its settings
        //0-> gestor
        //1-> funcionario
        if(u!=null && u.getType()==1){
            navigationView.getMenu().findItem(R.id.nav_meetings).setVisible(false);
        }

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        boolean chat_flag = sharedPref.getBoolean(getString(R.string.preference_file_key_chat),true);

        if(!chat_flag){
            navigationView.getMenu().findItem(R.id.nav_chat).setVisible(false);
        }

        View header=navigationView.getHeaderView(0);

        TextView username = (TextView) header.findViewById(R.id.menu_username);
        TextView email = (TextView) header.findViewById(R.id.menu_email);
        user_photo = (ImageView) header.findViewById(R.id.menu_user_pic);

        String photo = sharedPref.getString(getString(R.string.preference_file_key_user_photo),"");
        if(photo.equals("")){
            user_photo.setImageResource(R.drawable.ic_profile_avatar);
        }else{
            user_photo.setImageURI(Uri.parse(photo));
        }

        if(u!=null){
            username.setText(u.getUsername());
            email.setText(u.getEmail());
        }

        listUsersFragment = new ListUsersFragment();
        historyFragment = new HistoryFragment();
        manualClockInFragment = new ManualClockInFragment();
        settingsFragment = new SettingsFragment();
        meetingsFragment = new MeetingsFragment();
        profileFragment = new ProfileFragment();
        quickMenuFragment = new QuickMenuFragment();
        fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, quickMenuFragment).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            leave();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_menu) {
            fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, quickMenuFragment).commit();
            toolbar.setTitle(getResources().getString(R.string.title_activity_main_menu));
        } else if (id == R.id.nav_manual_clock_in) {
            fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, manualClockInFragment).commit();
            toolbar.setTitle(getResources().getString(R.string.title_clock_in));
        } else if (id == R.id.nav_history) {
            fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, historyFragment).commit();
            toolbar.setTitle(getResources().getString(R.string.title_history));
        } else if (id == R.id.nav_meetings) {
            fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, meetingsFragment).commit();
            toolbar.setTitle(getResources().getString(R.string.title_meeting));
        } else if (id == R.id.nav_chat) {
            fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, listUsersFragment).commit();
            toolbar.setTitle(getResources().getString(R.string.title_chat));
        } else if (id == R.id.nav_settings) {
            fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, settingsFragment).commit();
            toolbar.setTitle(getResources().getString(R.string.title_settings));
        } else if (id == R.id.nav_profile) {
            fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, profileFragment).commit();
            toolbar.setTitle(getResources().getString(R.string.title_profile));
        } else if (id == R.id.nav_logout) {
            logout();
        }else if (id == R.id.nav_leave) {
            leave();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void leave(){
        LogoutDialog ld = new LogoutDialog();

        Bundle args = new Bundle();
        args.putString("type", "leave");
        ld.setArguments(args);

        ld.show(getSupportFragmentManager(), "LogoutDialog");
    }

    public void logout(){
        LogoutDialog ld = new LogoutDialog();

        Bundle args = new Bundle();
        args.putString("type", "logout");
        ld.setArguments(args);

        ld.show(getSupportFragmentManager(), "LogoutDialog");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    profileFragment.setPhotoName(selectedImage.toString());
                }
                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    profileFragment.setPhotoName(selectedImage.toString());
                }
                break;
        }

        //initialize again fragment
        fragmentManager.beginTransaction().replace(R.id.content_menu_fragment_container, profileFragment).commit();
        toolbar.setTitle(getResources().getString(R.string.title_profile));
    }
}