package com.example.jm.clockin.Fragments;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.jm.clockin.Client.Tower;
import com.example.jm.clockin.Dialogs.ErrorDialog;
import com.example.jm.clockin.Dialogs.WorkersListDialog;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;
import com.example.jm.clockin.Adapter.WorkersAdapter;

import java.util.ArrayList;

public class MeetingsFragment extends Fragment {

    private AutoCompleteTextView autoCompleteTextView;
    private ArrayList<UsersModel> workers, all_workers;
    private View view;
    private ProgressBar mProgressView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_meetings, container, false);

        mProgressView = (ProgressBar) view.findViewById(R.id.meetings_progress);
        mProgressView.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);

        showProgress(true);
        (new MeetingsFragment.ContactSrvTask()).execute();

        return view;
    }

    private boolean existsUsername(String username){
        for(UsersModel u : all_workers){
            if(u.getUsername().equals(username))
                return true;
        }

        return false;
    }

    private void initList(ArrayList<UsersModel> u){
        autoCompleteTextView = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteTextView_workers);

        workers = u;

        if(u==null){
            setError();
            return;
        }

        all_workers = new ArrayList<>(workers);

        WorkersAdapter l = new WorkersAdapter(getContext(), workers);

        autoCompleteTextView.setAdapter(l);


        Button searchWorkers = (Button) view.findViewById(R.id.button_searchWorkers);
        searchWorkers.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                WorkersListDialog wld = new WorkersListDialog();
                Bundle args = new Bundle();
                args.putParcelableArrayList("workers", all_workers);
                wld.setArguments(args);
                wld.show(getFragmentManager(), "WorkersListDialog");
            }
        });

        Button create_meeting = (Button) view.findViewById(R.id.btn_create_meeting);
        create_meeting.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String username = autoCompleteTextView.getText().toString();
                if(!existsUsername(username)){
                    ErrorDialog err_dialog = new ErrorDialog();
                    Bundle args = new Bundle();
                    args.putString("text", getResources().getString(R.string.meeting_error_no_username));
                    err_dialog.setArguments(args);
                    err_dialog.show(getFragmentManager(), "ErrorDialog");
                }else{
                    showProgress(true);
                    (new ContactArduinoTask()).execute();
                }
            }
        });
    }

    private void setError() {
        ErrorDialog err_dialog = new ErrorDialog();
        Bundle args = new Bundle();
        args.putString("text", "Verifique a sua ligação à rede!");
        err_dialog.setArguments(args);
        err_dialog.show(getFragmentManager(), "ErrorDialog");
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showProgress(final boolean show) {
        if(show){
            mProgressView.setVisibility(View.VISIBLE);
            mProgressView.bringToFront();
        }else{
            mProgressView.setVisibility(View.GONE);
        }
    }

    public class ContactSrvTask extends AsyncTask<Void, Void, ArrayList<UsersModel>> {

        @Override
        protected ArrayList<UsersModel> doInBackground(Void... params) {
            return ((Tower) getActivity().getApplicationContext()).getOnlineUsers();
        }

        @Override
        protected void onPostExecute(final ArrayList<UsersModel> users) {
            showProgress(false);
            initList(users);
        }
    }

    public class ContactArduinoTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            ((Tower) getActivity().getApplicationContext()).addNewMeeting();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean post) {
           showProgress(false);
            if(getContext()!=null)
                Toast.makeText(getContext(), "Reunião agendada!", Toast.LENGTH_LONG).show();

        }

    }
}
