package com.example.jm.clockin.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.jm.clockin.Chat.ListUsers.ListUsersFragment;
import com.example.jm.clockin.Client.Tower;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;

public class QuickMenuFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quick_menu, container, false);

        Button btn_history = (Button) view.findViewById(R.id.quick_menu_history_btn);
        Button btn_meetings = (Button) view.findViewById(R.id.quick_menu_meetings_btn);
        Button btn_clock_in = (Button) view.findViewById(R.id.quick_menu_clock_in_btn);
        Button btn_chat = (Button) view.findViewById(R.id.quick_menu_chat_btn);

        UsersModel u = ((Tower) getActivity().getApplicationContext()).getUserLoggedIn();
        if(u.getType()==1){
            btn_meetings.setVisibility(View.GONE);
        }else{
            btn_meetings.setVisibility(View.VISIBLE);
        }

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        boolean chat_flag = sharedPref.getBoolean(getString(R.string.preference_file_key_chat),true);

        if(!chat_flag){
            btn_chat.setVisibility(View.GONE);
        }else{
            btn_chat.setVisibility(View.VISIBLE);
        }
        
        btn_history.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                HistoryFragment historyFragment = new HistoryFragment();
                getFragmentManager().beginTransaction().replace(R.id.content_menu_fragment_container, historyFragment).commit();
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.title_history));
            }
        });

        btn_meetings.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MeetingsFragment meetingsFragment = new MeetingsFragment();
                getFragmentManager().beginTransaction().replace(R.id.content_menu_fragment_container, meetingsFragment).commit();
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.title_meeting));
            }
        });

        btn_clock_in.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ManualClockInFragment manualClockInFragment = new ManualClockInFragment();
                getFragmentManager().beginTransaction().replace(R.id.content_menu_fragment_container, manualClockInFragment).commit();
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.title_clock_in));
            }
        });

        btn_chat.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ListUsersFragment listUsersFragment = new ListUsersFragment();
                getFragmentManager().beginTransaction().replace(R.id.content_menu_fragment_container, listUsersFragment).commit();
                ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.title_chat));
            }
        });

        return view;
    }

}
