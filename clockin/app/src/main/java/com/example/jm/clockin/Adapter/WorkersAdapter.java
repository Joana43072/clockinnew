package com.example.jm.clockin.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.jm.clockin.Models.MessageModel;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;

import java.util.ArrayList;

public class WorkersAdapter extends ArrayAdapter{

    private static final String EMPTY = "";
    private static final String SEM_INFO = "Sem informação";

    private Context context;
    final private ArrayList<UsersModel> users, tempUsers, suggestions;

    public WorkersAdapter(Context context, ArrayList<UsersModel> list){
        super(context, android.R.layout.simple_list_item_1, list);

        this.context = context;
        this.users = list;
        this.tempUsers = new ArrayList<>(list);
        this.suggestions = new ArrayList<>(list);
    }

    /**
     * Método que mostra cada item de uma lista, caso esteja a mostrar o último elemento, se ainda existirem
     * mais páginas para listar, apresentar a barra de espera, caso contrário, apresentar apenas o último elemento.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UsersModel item = users.get(position);
        convertView = LayoutInflater.from(this.context).inflate(R.layout.workers_list_item_dialog, null);

        TextView text_name = (TextView) convertView.findViewById(R.id.worker_name);
        String name = item.getUsername();
        text_name.setText(name);

        return convertView;

    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }

    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            UsersModel user = (UsersModel) resultValue;
            return user.getUsername();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (UsersModel people : tempUsers) {
                    if (people.getUsername().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<UsersModel> c = (ArrayList<UsersModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (UsersModel cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            }
            else{
                clear();
                notifyDataSetChanged();
            }
        }
    };
}
