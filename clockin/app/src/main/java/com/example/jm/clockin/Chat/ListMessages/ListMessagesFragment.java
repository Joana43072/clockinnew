package com.example.jm.clockin.Chat.ListMessages;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.jm.clockin.Adapter.MessagesAdapter;
import com.example.jm.clockin.Chat.ListUsers.ListUsersFragment;
import com.example.jm.clockin.Client.Tower;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.Models.MessageModel;
import com.example.jm.clockin.R;

import java.util.ArrayList;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ListUsersFragment}
 * in two-pane mode (on tablets) or a {@link ListMessagesActivity}
 * on handsets.
 */
public class ListMessagesFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM = "item";

    private ListView lv;
    private MessagesAdapter l;
    private ArrayList<MessageModel> list;
    private UsersModel user_to;
    private ProgressBar mProgressView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListMessagesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM)) {
            user_to = getArguments().getParcelable(ARG_ITEM);
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(user_to.getUsername());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.chat_list_messages_list, container, false);

        mProgressView = (ProgressBar) rootView.findViewById(R.id.chat_list_messages_progress);
        mProgressView.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);

        list = new ArrayList<>();

        lv = (ListView) rootView.findViewById(R.id.list_messages);

        l = new MessagesAdapter(getContext(), list);

        lv.setAdapter(l);

        showProgress(true);
        (new ListMessagesFragment.ContactSrvTask(user_to.getId())).execute();

        return rootView;
    }


    private void updateList(final ArrayList<MessageModel> messages) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list = messages;
                l = new MessagesAdapter(getContext(), list);
                synchronized(lv){
                    lv.setAdapter(l);
                    showProgress(false);
                }
            }
        });
    }

    public void notifyList(){
        showProgress(true);
        (new ListMessagesFragment.ContactSrvTask(user_to.getId())).execute();
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showProgress(final boolean show) {
        if(show){
            mProgressView.setVisibility(View.VISIBLE);
            mProgressView.bringToFront();
        }else{
            mProgressView.setVisibility(View.GONE);
        }
    }

    public class ContactSrvTask extends AsyncTask<Void, Void, ArrayList<MessageModel>> {

        private final int id1;

        ContactSrvTask(int id1) {
            this.id1 = id1;
        }

        @Override
        protected ArrayList<MessageModel> doInBackground(Void... params) {
            return ((Tower) getActivity().getApplicationContext()).getMessagesConvo(id1);
        }

        @Override
        protected void onPostExecute(final ArrayList<MessageModel> messages) {
            updateList(messages);
        }

    }
}
