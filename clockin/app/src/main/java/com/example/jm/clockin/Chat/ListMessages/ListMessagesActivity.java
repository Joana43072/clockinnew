package com.example.jm.clockin.Chat.ListMessages;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.jm.clockin.LoadActivity;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;
import com.example.jm.clockin.Client.Tower;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ListMessagesActivity extends AppCompatActivity {

    private ListMessagesFragment fragment;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.chat_list_messages_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        fragment = new ListMessagesFragment();

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            final UsersModel user = getIntent().getParcelableExtra(ListMessagesFragment.ARG_ITEM);
            arguments.putParcelable(ListMessagesFragment.ARG_ITEM, user);

            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();

            final EditText message = (EditText) findViewById(R.id.text_message);
            final Button save = (Button) findViewById(R.id.btn_send_message);

            message.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(s.toString().trim().length()==0){
                        save.setEnabled(false);
                        save.setBackground(getResources().getDrawable(R.drawable.ic_send_btn_5_dissabled));
                    } else {
                        save.setEnabled(true);
                        save.setBackground(getResources().getDrawable(R.drawable.ic_send_btn_5));
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void afterTextChanged(Editable s) {}
            });

            save.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                    String timestamp = df.format(Calendar.getInstance().getTime());
                    int id_to = user.getId();
                    String text = message.getText().toString();
                    message.setText("");
                    (new ListMessagesActivity.ContactSrvSendMessageTask(id_to, text, timestamp)).execute();
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, LoadActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void notifyListToUpdate() {
        if(fragment!=null)
            fragment.notifyList();
    }

    public class ContactSrvSendMessageTask extends AsyncTask<Void, Void, Boolean> {

        private final int id_to;
        private final String text, timestamp;

        ContactSrvSendMessageTask(int id_to, String text, String timestamp) {
            this.id_to = id_to;
            this.text = text;
            this.timestamp = timestamp;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            ((Tower) getApplicationContext()).sendMessage(id_to, text, timestamp);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            notifyListToUpdate();
        }
    }



}
