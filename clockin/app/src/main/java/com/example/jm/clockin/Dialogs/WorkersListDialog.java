package com.example.jm.clockin.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;

import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;
import com.example.jm.clockin.Adapter.WorkersAdapter;

import java.util.ArrayList;

public class WorkersListDialog extends android.support.v4.app.DialogFragment {

    private ArrayList<UsersModel> workers_online;
    ListView lv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.workers_list_dialog, null);

        workers_online = getArguments().getParcelableArrayList("workers");
        final AutoCompleteTextView name_worker = (AutoCompleteTextView)getActivity().findViewById(R.id.autoCompleteTextView_workers);

        getDialog().setTitle(getResources().getString(R.string.meeting_dialog_online_users));
        lv = (ListView) v.findViewById(R.id.workers_dialog_list);
        WorkersAdapter adapter = new WorkersAdapter(getContext(), workers_online);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        name_worker.setText(workers_online.get(position).getUsername());
                        getDialog().dismiss();
                    }
                }
        );

        Button btn_cancel = (Button) v.findViewById(R.id.workers_list_dialog_button_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener()
        {   @Override
        public void onClick(View v){
            getDialog().dismiss();
        }
        });
        return v;

    }
}
