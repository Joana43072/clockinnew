package com.example.jm.clockin.Models;

import java.io.Serializable;

public class RecordModel implements Serializable {
    private static final long serialVersionUID = 1L;

    private String timestamp;

    private int id, type, confirmed, user_id;

    public RecordModel(int id, int type, int user_id, String timestamp, int confirmed){
        this.id = id;
        this.type=type;
        this.timestamp = timestamp;
        this.confirmed = confirmed;
        this.user_id = user_id;
    }

    public int getType(){return type;}

    public String getTimestamp(){return timestamp;}

    public int getConfirmed(){return confirmed;}

    public void setConfirmed(int confirmed){this.confirmed=confirmed;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
