package com.example.jm.clockin.Client;

import android.app.FragmentManager;
import android.content.Context;
import android.net.http.HttpResponseCache;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.jm.clockin.Dialogs.ErrorDialog;
import com.example.jm.clockin.Models.MessageModel;
import com.example.jm.clockin.Models.RecordModel;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;


public class ContactServer {

    /*
    Constantes usadas na construção do url
     */
    private static final String BASE_URL = "http://192.168.1.67:8080/ClockIn/";
    private static final String MESSAGES = "message/";
    private static final String USER = "user/";
    private static final String RECORDS = "cks";
    private static final String NEW ="message";

    private static final String signinURL = BASE_URL+"auth/signin";
    private static final String signoffURL = BASE_URL+"auth/signoff";
    private static final String onlineUsersURL = BASE_URL+"user/online";
    private static final String messagesRangeURL = BASE_URL+"message/%d/conv/%d";
    private static final String setUserNameURL = BASE_URL+"user/%s/update/nm/%s";
    private static final String setUserPhoneURL = BASE_URL+"user/%s/update/nr/%s";
    private static final String addClockInURL = BASE_URL+"user/%d/cks";
    private static final String addMessageURL = BASE_URL+MESSAGES+"new";

    private ParseEntities parseEntities;
    private Context c;

    public ContactServer(Context c){
        parseEntities = new ParseEntities();
        this.c = c;
    }

    public UsersModel signIn(String username, String password){

        try {
            String url = signinURL;
            JSONObject auth   = new JSONObject();
            auth.put("username",username);
            auth.put("password", password);

            StringBuilder builder = executePost(url, auth);

            if(builder==null)
                return null;

            //failed signIn
            if(builder.length()==0)
                return null;

            JSONObject jsonObject = new JSONObject(builder.toString());
            UsersModel user = parseEntities.parseUser(jsonObject);

            if(user!=null)
                return user;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    public boolean signOut(int id){

        try {
            String url = signoffURL;
            JSONObject auth   = new JSONObject();
            auth.put("id",id+"");
            executePost(url, auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<UsersModel> getOnlineUsers() {
        String url = onlineUsersURL;
        StringBuilder builder = executeGet(url);

        if(builder==null)
            return null;

        ArrayList<UsersModel> users = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(builder.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                UsersModel user = parseEntities.parseUser(jsonObject);

                if(user!=null)
                    users.add(user);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return users;
    }

    public boolean setUserName(int id, String newName){
        String url = String.format(setUserNameURL, id, newName);
        executePost(url, new JSONObject());
        return true;
    }

    public boolean setPhone(int id, int phone){
        String url = String.format(setUserPhoneURL, id, phone);
        executePost(url, new JSONObject());
        return true;
    }

    public int newClockIn(int id, String timestamp, int status, int type){
        String url = String.format(addClockInURL, id);

        try {
            JSONObject record   = new JSONObject();
            record.put("timestamp",timestamp);
            record.put("status",status);
            record.put("type",type);

            StringBuilder builder = executePost(url, record);
            System.err.println(">>>>>>>> builder" + builder.toString());
            if(builder==null)
                return 6;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public boolean newMessage(int idFrom, int idTo, String text, String timestamp){
        try {
            String url = addMessageURL;
            JSONObject message = new JSONObject();
            message.put("idFrom",idFrom);
            message.put("idTo",idTo);
            message.put("text", text);
            message.put("timestamp",timestamp);

            executePost(url, message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<MessageModel> getMessages(int user1, int user2){
        String url = String.format(messagesRangeURL, user1, user2);
        StringBuilder builder = executeGet(url);

        if(builder==null)
            return null;

        ArrayList<MessageModel> messages = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(builder.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MessageModel message = parseEntities.parseMessage(jsonObject);

                if(message!=null)
                    messages.add(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return messages;
    }

    public ArrayList<RecordModel> getUserClockIn(int id, String b_timestamp, String e_timestamp){
        String[] begin = b_timestamp.split(" ");
        String beginD = begin[0];
        String beginT = begin[1];

        String[] end = e_timestamp.split(" ");
        String endD = end[0];
        String endT = end[1];

        String url = BASE_URL+USER+id+"/"+RECORDS+"?beginD="+beginD+"&beginT="+beginT+"&endD="+endD+"&endT="+endT;
        StringBuilder builder = executeGet(url);

        if(builder==null){
            return null;
        }


        ArrayList<RecordModel> records = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(builder.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                RecordModel record = parseEntities.parseRecord(jsonObject);

                if(record!=null)
                    records.add(record);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return records;
    }

    private StringBuilder executeGet(String url) {
        System.err.println(">>>>>>>>>> GET: "+url);
        try{
            HttpClient httpClient=new DefaultHttpClient();

            HttpGet httpGet=new HttpGet(url);
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();

            if (entity != null) {

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                    builder.append(line).append("\n");
                }

                return builder;
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
        return null;
    }

    private StringBuilder executePost(String url, JSONObject jsonObject) {
        System.err.println(">>>>>>>>>> POST: "+url);
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 3000000);
        HttpResponse response;

        try {

            HttpPost post = new HttpPost(url);

            StringEntity se = new StringEntity( jsonObject.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);

            response = client.execute(post);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            StringBuilder builder = new StringBuilder();

            for (String line = null; (line = reader.readLine()) != null;) {
                builder.append(line).append("\n");
            }
            return builder;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
