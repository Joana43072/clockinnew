package com.example.jm.clockin.Client;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.http.HttpResponseCache;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.example.jm.clockin.Dialogs.ErrorDialog;
import com.example.jm.clockin.Fragments.ManualClockInFragment;
import com.example.jm.clockin.Models.MessageModel;
import com.example.jm.clockin.Models.RecordModel;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class Tower extends Application {

    private ContactServer srv;
    private ContactArduino arduino;
    private static final int ENTRADA = 1;
    private static final int SAIDA = 0;
    private static final int SUCESSO = 1;
    private static final int INVALID_TIME = 2;
    private static final int ALREADY_CLOCK_IN = 3;
    private static final int NULL_USER = 4;
    private static final int NOT_ACTIVE = 5;
    private static final int NOT_CONNECTED = 6;
    private static final int NOT_CONFIRMED = 0;

    UsersModel userLoggedIn;
    Map<String, ArrayList<RecordModel>> records;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public Tower(){
        srv = new ContactServer(this.getBaseContext());
        arduino = new ContactArduino();
        records = new HashMap<>();
        userLoggedIn = null;
    }

    public boolean isLoggedIn(){
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = mPrefs.getString(getString(R.string.preference_file_key_user_logged_in), "");
        UsersModel obj;
        if(json!=""){
            obj = gson.fromJson(json, UsersModel.class);
        }else{
            return false;
        }
        if(obj!=null){
            userLoggedIn = obj;
            return true;
        }
        return false;
    }

    public boolean login(String username, String pwd){
        UsersModel user = srv.signIn(username, pwd);

        if(user == null)
            return false;

        userLoggedIn = user;

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString(getString(R.string.preference_file_key_user_logged_in), json);
        prefsEditor.commit();

        return true;
    }

    public UsersModel getUserLoggedIn(){
        return userLoggedIn;
    }

    public void setUserLoggedInName(String name){
        UsersModel user = userLoggedIn;
        if(user==null)
            return;
        user.setName(name);
        srv.setUserName(user.getId(), name);
        userLoggedIn = user;
    }

    public void setUserLoggedInPhone(int phone){
        UsersModel user = userLoggedIn;
        if(user==null)
            return;
        user.setPhone_nr(phone);
        srv.setPhone(user.getId(), phone);
        userLoggedIn = user;
    }

    public ArrayList<UsersModel> getOnlineUsers() {
        return srv.getOnlineUsers();
    }

    public ArrayList<RecordModel> getUserRecords(String from, String to){
        ArrayList<RecordModel> records = getHistorySearch(from, to);

        if(records!=null)
            return records;

        ArrayList<RecordModel> records2 = new ArrayList<>();

        if(userLoggedIn!=null)
            records2 = srv.getUserClockIn(userLoggedIn.getId(), from, to);

        if(records!=null)
            setHistorySearch(from, to, records);

        return records2;
    }

    public ArrayList<UsersModel> getOnlineUsersMessagesWithLoggedInUser(){
        ArrayList<UsersModel> users = srv.getOnlineUsers();
        for(UsersModel u:users){
            if(u.getUsername().equals(userLoggedIn.getUsername())) {
                users.remove(u);
                break;
            }
        }
         return users;
    }

    public ArrayList<MessageModel> getMessagesConvo(int id_user1){
        if(userLoggedIn!=null)
            return srv.getMessages(id_user1, userLoggedIn.getId());
        return new ArrayList<>();
    }

    public ArrayList<RecordModel> getTodayRecords() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String today = df.format(c.getTime());
        String b_timestamp = today+" 00:00";
        String e_timestamp = today+" 23:59";
        return getUserRecords(b_timestamp, e_timestamp);
    }

    public void addNewMeeting() {
        if(userLoggedIn!=null)
        arduino.addMeeting(userLoggedIn.getOffice());
    }

    private boolean testTimeInterval(int type, String time){
        try{
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            DateFormat date = new SimpleDateFormat("HH:mm");
            Date today_date = date.parse(time);

            if(type==ENTRADA){
                String ent_from = sharedPref.getString(getString(R.string.preference_file_key_ent_from),"00:00");
                String ent_to = sharedPref.getString(getString(R.string.preference_file_key_ent_to),"14:59");
                Date ent_from_date = date.parse(ent_from);
                Date ent_to_date = date.parse(ent_to);
                if(today_date.before(ent_from_date) || today_date.after(ent_to_date)){
                    return false;
                }
                return true;

            }else{
                String sai_from = sharedPref.getString(getString(R.string.preference_file_key_sai_from),"15:00");
                String sai_to = sharedPref.getString(getString(R.string.preference_file_key_sai_to),"23:59");
                Date sai_from_date = date.parse(sai_from);
                Date sai_to_date = date.parse(sai_to);

                if(today_date.before(sai_from_date) || today_date.after(sai_to_date)){
                    return false;
                }
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;

        }
    }

    public int addNewAutoRecord(String time) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean active = sharedPref.getBoolean(getString(R.string.preference_file_key_reg_aut),true);


        if(!active)
            return NOT_ACTIVE;

        if(!this.testTimeInterval(ENTRADA, time))
            return INVALID_TIME;

        return this.addNewRecord(ENTRADA, time, NOT_CONFIRMED);

    }

    public int addNewRecord(int type, String time, int confirmed) {
        if(this.isAlreadyClockedIn(type))
            return ALREADY_CLOCK_IN;

        Calendar c = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String today = df.format(c.getTime())+" "+time;

        if(userLoggedIn==null)
            return NULL_USER;

        int message = srv.newClockIn(userLoggedIn.getId(), today, confirmed, type);
        if(message==NOT_CONNECTED)
            return NOT_CONNECTED;
        invalidateRecords();

        return SUCESSO;

    }

    private boolean isAlreadyClockedIn(int type) {
        List<RecordModel> records = getTodayRecords();

        if(records!=null){
            for(RecordModel r : records){
                if(r.getType()==type)
                    return true;
            }
        }

        return false;
    }

    public void sendMessage(int to, String message, String timestamp){
        if(userLoggedIn!=null)
            srv.newMessage(userLoggedIn.getId(), to, message, timestamp);
    }

    public void logout() {
        if(userLoggedIn!=null){
            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor prefsEditor = mPrefs.edit();
            prefsEditor.putString(getString(R.string.preference_file_key_user_logged_in), "");
            prefsEditor.commit();
            srv.signOut(userLoggedIn.getId());
        }

    }

    private ArrayList<RecordModel> getHistorySearch(String from, String to){
        System.err.println(">>>>>>>>>> GET CACHE: "+from+";"+to);
        ArrayList<RecordModel> r = records.get(from+";"+to);
        System.err.println("");
        return r;
    }

    private void setHistorySearch(String from, String to, ArrayList<RecordModel> m){
        System.err.println(">>>>>>>>>> SET CACHE: "+from+";"+to+"      "+m);
        records.put(from+";"+to, m);
    }

    private void invalidateRecords(){
        records.clear();
    }
}
