package com.example.jm.clockin.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.jm.clockin.Models.MessageModel;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;

import java.util.ArrayList;


public class UsersAdapter extends ArrayAdapter{

    private Context context;
    private ArrayList<UsersModel> list;
    private boolean load;

    private static final String SEM_INFO = "Sem informação";
    private static final String EMPTY = "";

    public UsersAdapter(Context context, ArrayList<UsersModel> list){
        super(context, 0, list);
        this.context = context;
        this.list = list;
        this.load = true;
    }

    /**
     * Método que indica se a barra de espera deve ser escondida ou ativada.
     * @param state: true caso se queira mostrar a barra de espera, false c.c.
     */
    public void setLoad(boolean state){
        load = state;
    }

    /**
     * Método que mostra cada item de uma lista, caso esteja a mostrar o último elemento, se ainda existirem
     * mais páginas para listar, apresentar a barra de espera, caso contrário, apresentar apenas o último elemento.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UsersModel item = list.get(position);
        convertView = LayoutInflater.from(this.context).inflate(R.layout.chat_list_users_item, null);

        TextView t1 = (TextView) convertView.findViewById(R.id.users_list_username);
        String username = item.getUsername();
        if (!username.equals(EMPTY)) {
            t1.setText(username);
        } else {
            t1.setText(SEM_INFO);
        }
        if((position % 2)==0){
            convertView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        }
        return convertView;
    }
}
