package com.example.jm.clockin.Client;

import com.example.jm.clockin.Models.MessageModel;
import com.example.jm.clockin.Models.RecordModel;
import com.example.jm.clockin.Models.UsersModel;

import org.json.JSONObject;

import java.util.ArrayList;


public class ParseEntities {
        /*
    Constantes usadas nos campos dos jsons
     */
    private static final String ID = "id";
    //messages
    private static final String MESSAGE_MESSAGE = "text";
    private static final String MESSAGE_SENDER = "from";
    private static final String MESSAGE_RECEIVER = "to";
    private static final String MESSAGE_TIMESTAMP = "timeStamp";

    //user
    private static final String USER_ARDUINO_ID = "arduino";
    private static final String USER_POST = "post";
    private static final String USER_NAME = "name";
    private static final String USER_EMAIL = "email";
    private static final String USER_TYPE = "type";
    private static final String USER_USERNAME = "username";
    private static final String USER_PASSWORD = "password";
    private static final String USER_PHONE_NR = "phoneNumber";
    private static final String USER_OFFICE = "office";
    private static final String USER_PHOTO = "photo";

    private static final String RECORD_TYPE = "type";
    private static final String RECORD_STATUS = "status";
    private static final String RECORD_TIMESTAMP = "timeStamp";
    private static final String RECORD_USER = "user";

    public ParseEntities(){}

    public UsersModel parseUser(JSONObject jsonObject) {
        String username, name, email, photo, office;
        username=name=email=photo=office="";
        int online = 1;
        int phone_nr, type, id;
        phone_nr=type=id=-1;
        ArrayList<MessageModel> messages = null;

        System.err.println(">>>>>>>>>> PARSE USER: "+jsonObject.toString());

        try{
            if(jsonObject.has(ID))
                id = jsonObject.getInt(ID);

            if(jsonObject.has(USER_USERNAME))
                username = jsonObject.getString(USER_USERNAME);

            if(jsonObject.has(USER_NAME))
                name = jsonObject.getString(USER_NAME);

            if(jsonObject.has(USER_EMAIL))
                email = jsonObject.getString(USER_EMAIL);

            if(jsonObject.has(USER_PHONE_NR))
                phone_nr = jsonObject.getInt(USER_PHONE_NR);

            if(jsonObject.has(USER_TYPE))
                type = jsonObject.getInt(USER_TYPE);

            if(jsonObject.has(USER_PHOTO))
                photo = jsonObject.getString(USER_PHOTO);

            if(jsonObject.has(USER_OFFICE))
                office = jsonObject.getString(USER_OFFICE);

        }catch (Exception e){
            e.printStackTrace();
        }


        return new UsersModel(id, username, name, phone_nr, online, messages, email, type, photo, office);
    }

    public MessageModel parseMessage(JSONObject jsonObject) {
        String text, timeStamp;
        text=timeStamp="";
        int id, from, to;
        id = from = to = -1;

        System.err.println(">>>>>>>>>> PARSE MESSAGE: "+jsonObject.toString());

        try{
            if(jsonObject.has(ID))
                id = jsonObject.getInt(ID);

            if(jsonObject.has(MESSAGE_RECEIVER))
                to = jsonObject.getInt(MESSAGE_RECEIVER);

            if(jsonObject.has(MESSAGE_SENDER))
                from = jsonObject.getInt(MESSAGE_SENDER);

            if(jsonObject.has(MESSAGE_TIMESTAMP))
                timeStamp = jsonObject.getString(MESSAGE_TIMESTAMP);

            if(jsonObject.has(MESSAGE_MESSAGE))
                text = jsonObject.getString(MESSAGE_MESSAGE);

            return new MessageModel(id, from, to, timeStamp, text);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public RecordModel parseRecord(JSONObject jsonObject) {
        String timeStamp="";
        int id, type, status, user;
        id=type=status=user=-1;

        System.err.println(">>>>>>>>>> PARSE RECORD: "+jsonObject.toString());

        try{
            if(jsonObject.has(ID))
                id = jsonObject.getInt(ID);

            if(jsonObject.has(RECORD_TYPE))
                type = jsonObject.getInt(RECORD_TYPE);

            if(jsonObject.has(RECORD_STATUS))
                status = jsonObject.getInt(RECORD_STATUS);

            if(jsonObject.has(RECORD_USER))
                user = jsonObject.getInt(RECORD_USER);

            if(jsonObject.has(RECORD_TIMESTAMP))
                timeStamp = jsonObject.getString(RECORD_TIMESTAMP);


            return new RecordModel(id, type, user, timeStamp, status);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
