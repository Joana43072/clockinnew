package com.example.jm.clockin.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.jm.clockin.Models.RecordModel;
import com.example.jm.clockin.R;

import java.util.ArrayList;


public class RecordsAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<RecordModel> list;
    private boolean load;
    private ProgressBar loading;

    private static final String SEM_INFO = "Sem informação";
    private static final String EMPTY = "";

    public RecordsAdapter(Context context, ArrayList<RecordModel> list){
        super(context, 0, list);
        this.context = context;
        this.list = list;
        this.load = true;
    }

    /**
     * Método que indica se a barra de espera deve ser escondida ou ativada.
     * @param state: true caso se queira mostrar a barra de espera, false c.c.
     */
    public void setLoad(boolean state){
        load = state;
    }

    /**
     * Método que mostra cada item de uma lista, caso esteja a mostrar o último elemento, se ainda existirem
     * mais páginas para listar, apresentar a barra de espera, caso contrário, apresentar apenas o último elemento.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            RecordModel item = list.get(position);
            convertView = LayoutInflater.from(this.context).inflate(R.layout.item_list_records, null);

            TextView t1 = (TextView) convertView.findViewById(R.id.item_list_record_type);
            int type = item.getType();
            if(type==0){
                t1.setText(this.context.getResources().getString(R.string.clock_in_entry_type_leave));
            }else{
                t1.setText(this.context.getResources().getString(R.string.clock_in_entry_type_entry));
            }

            TextView t2 = (TextView) convertView.findViewById(R.id.item_list_record_date_time);
            String date = item.getTimestamp();
            t2.setText(date+EMPTY);

            TextView t3 = (TextView) convertView.findViewById(R.id.item_list_record_confirmed);
            int confirmed = item.getConfirmed();
            if(confirmed==0){
                t3.setText(this.context.getResources().getString(R.string.general_label_not_confirmed));
                t3.setTextColor(getContext().getResources().getColor(R.color.errorColor));
            }else if(confirmed==1){
                t3.setText(this.context.getResources().getString(R.string.general_label_confirmed));
                t3.setTextColor(getContext().getResources().getColor(R.color.validColor));
            }else{
                t3.setText(SEM_INFO);
            }

            if((position % 2)==0){
                convertView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
            }

            return convertView;


    }
}
