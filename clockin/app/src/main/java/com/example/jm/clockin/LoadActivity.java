package com.example.jm.clockin;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.jm.clockin.Client.Tower;

public class LoadActivity extends AppCompatActivity {

    private boolean left;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        new WifiBroadcastReceiver(this).checkStatus();

        left = false;

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!left)
                    initApp();
            }
        }, 3000L);
    }

    public void init(View view){
        initApp();
    }

    public void initApp(){
        left = true;
        if(((Tower) getApplicationContext()).isLoggedIn()){
            System.err.println(">>>>>>>>>>>>>>>>>>> LOAD user already logged in");
            startActivity(new Intent(this, MainMenuActivity.class));
        }else{
            System.err.println(">>>>>>>>>>>>>>>>>>> LOAD user not logged in");
            startActivity(new Intent(this, LoginActivity.class));
        }
        finish();
    }

}
