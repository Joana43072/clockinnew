package com.example.jm.clockin.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;


public class UsersModel implements Parcelable {

    private String username, name, email, photo, office;
    private int id, phone_nr, type, online;
    private ArrayList<MessageModel> messages;

    public UsersModel(int id, String username, String name, int phone_nr, int online, ArrayList<MessageModel> messages,
                      String email, int type, String photo, String office){
        this.id = id;
        this.username = username;
        this.name = name;
        this.phone_nr = phone_nr;
        this.online = online;
        this.messages = messages;
        this.email = email;
        this.type = type;
        this.photo = photo;
        this.office = office;
    }

    protected UsersModel(Parcel in) {
        id = in.readInt();
        username = in.readString();
        online = in.readInt();
        messages = in.createTypedArrayList(MessageModel.CREATOR);
    }

    public static final Creator<UsersModel> CREATOR = new Creator<UsersModel>() {
        @Override
        public UsersModel createFromParcel(Parcel in) {
            return new UsersModel(in);
        }

        @Override
        public UsersModel[] newArray(int size) {
            return new UsersModel[size];
        }
    };

    public int getId(){return id;}

    public String getUsername(){return username;}

    public String getName(){return name;}

    public void setName(String name){this.name=name;}

    public int getPhone_nr(){return phone_nr;}

    public void setPhone_nr(int phone){this.phone_nr=phone_nr;}

    public int getOnline(){return online;}

    public ArrayList<MessageModel> getMessages(){return messages;}

    public void setMessages(ArrayList<MessageModel> messages){
        this.messages = messages;
    }

    public String getEmail() {
        return email;
    }

    public int getType() {
        return type;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(username);
        dest.writeInt(online);
        dest.writeTypedList(messages);
    }
}
