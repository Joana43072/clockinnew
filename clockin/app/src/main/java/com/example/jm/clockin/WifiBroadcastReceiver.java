package com.example.jm.clockin;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;

import com.example.jm.clockin.Client.Tower;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.content.Context.NOTIFICATION_SERVICE;


public class WifiBroadcastReceiver extends BroadcastReceiver {
    private Context context;
    private static final String SSID_openfct = "\"OpenFCT\"";
    private static final String SSID_mariana = "\"MEO-456339\"";
    private static final int ENTRADA = 1;
    private static final int NOT_CONFIRMED = 0;
    private static final int SUCESSO = 1;

    public WifiBroadcastReceiver(Context context){
        this.context = context;
    }

    public WifiBroadcastReceiver(){}

    @Override
    public void onReceive(Context context, Intent intent) {
        System.err.print(">>>>>>>>>>> CONNECTION CHANGED  ");
        this.context = context;
        checkStatus();
    }

    public void checkStatus(){

        System.err.print(">>>>>>>>>>> TEST CONNECTION  ");
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo;

        wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
            String ssid = wifiInfo.getSSID();
            if(isConnected && (ssid.equals(SSID_openfct) || ssid.equals(SSID_mariana)))
                tryClockIn();
            System.err.print("connected? "+isConnected);
            System.err.println(" newtork SSDI: "+ssid);
        }
    }

    private void tryClockIn() {
        System.err.println(">>>>>>>>>> TRY CLOCKIN");
        DateFormat df = new SimpleDateFormat("HH:mm");
        String time = df.format(Calendar.getInstance().getTime());
        (new ContactSrvAddClockInTask(ENTRADA, NOT_CONFIRMED, time)).execute();
    }

    public class ContactSrvAddClockInTask extends AsyncTask<Void, Void, Integer> {

        private final int type, status;
        private final String timestamp;

        ContactSrvAddClockInTask(int type, int status, String timestamp){
            this.type = type;
            this.status = status;
            this.timestamp = timestamp;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return ((Tower) context.getApplicationContext()).addNewAutoRecord(timestamp);
        }

        @Override
        protected void onPostExecute(final Integer b) {
            if(b!=SUCESSO)
                return;

            Intent intent = new Intent(context, WifiBroadcastReceiver.class);
            PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
            Notification n  = new Notification.Builder(context)
                    .setContentTitle("Registo automático")
                    .setContentText("Foi efetuado um registo de entrada automaticamente que deverá confirmar na sua estação Arduino.")
                    .setSmallIcon(R.drawable.logo_small)
                    .setContentIntent(pIntent)
                    .setAutoCancel(true).build();

            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(0, n);
        }

    }

}
