package com.example.jm.clockin.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.jm.clockin.Client.Tower;
import com.example.jm.clockin.Models.MessageModel;
import com.example.jm.clockin.R;

import java.util.ArrayList;

public class MessagesAdapter extends ArrayAdapter{

    private Context context;
    private ArrayList<MessageModel> list;
    private int idLoggedInUser;

    private static final String SEM_INFO = "Sem informação";
    private static final String EMPTY = "";

    public MessagesAdapter(Context context, ArrayList<MessageModel> list){
        super(context, 0, list);
        this.context = context;
        this.list = list;
        idLoggedInUser = ((Tower) getContext().getApplicationContext()).getUserLoggedIn().getId();
    }

    /**
     * Método que mostra cada item de uma lista, caso esteja a mostrar o último elemento, se ainda existirem
     * mais páginas para listar, apresentar a barra de espera, caso contrário, apresentar apenas o último elemento.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MessageModel item = list.get(position);
        convertView = LayoutInflater.from(this.context).inflate(R.layout.chat_list_messages_item, null);
        LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.message_id_layout);


        TextView t1 = (TextView) convertView.findViewById(R.id.id);
        String message = item.getMessage();
        t1.setText(message);


        TextView t2 = (TextView) convertView.findViewById(R.id.content);
        String timestamp = item.getTimestamp();
        if(!timestamp.equals(EMPTY)){
            t2.setText(timestamp);
        }else{
            t2.setText(SEM_INFO);
        }

        if(item.getSender()==idLoggedInUser){
            t1.setGravity(Gravity.RIGHT);
            t2.setGravity(Gravity.RIGHT);
            if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                layout.setBackgroundDrawable( context.getResources().getDrawable(R.drawable.message_sent) );
            } else {
                layout.setBackground( context.getResources().getDrawable(R.drawable.message_sent));
            }
        }else{
            t1.setGravity(Gravity.LEFT);
            t2.setGravity(Gravity.LEFT);
            if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                layout.setBackgroundDrawable( context.getResources().getDrawable(R.drawable.message_received) );
            } else {
                layout.setBackground( context.getResources().getDrawable(R.drawable.message_received));
            }
        }

        return convertView;

    }
}
