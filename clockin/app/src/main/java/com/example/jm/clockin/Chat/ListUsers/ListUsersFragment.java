package com.example.jm.clockin.Chat.ListUsers;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.example.jm.clockin.Adapter.UsersAdapter;
import com.example.jm.clockin.Chat.ListMessages.ListMessagesActivity;
import com.example.jm.clockin.Chat.ListMessages.ListMessagesFragment;
import com.example.jm.clockin.Dialogs.ErrorDialog;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;
import com.example.jm.clockin.Client.Tower;

import java.util.ArrayList;


public class ListUsersFragment extends Fragment {

    private ListView lv;
    private UsersAdapter l;
    private ArrayList<UsersModel> list;
    private View main_view;
    private ProgressBar mProgressView;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        main_view = inflater.inflate(R.layout.chat_list_users_page, container, false);

        mProgressView = (ProgressBar) main_view.findViewById(R.id.chat_list_users_progress);
        mProgressView.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);

        showProgress(true);
        (new ListUsersFragment.ContactSrvTask()).execute();

        return main_view;
    }

    private void setError() {
        ErrorDialog err_dialog = new ErrorDialog();
        Bundle args = new Bundle();
        args.putString("text", "Verifique a sua ligação à rede!");
        err_dialog.setArguments(args);
        err_dialog.show(getFragmentManager(), "ErrorDialog");
    }

    private void initList(ArrayList<UsersModel> u){

        if(u==null){
            this.setError();
            return;
        }

        list = u;

        lv = (ListView) main_view.findViewById(R.id.list_users);

        l = new UsersAdapter(getContext(), list);

        lv.setAdapter(l);


        //para depois abrir os detalhes de cada item da lista
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UsersModel item = list.get(position);
                Context context = getActivity().getApplicationContext();
                Intent intent = new Intent(context, ListMessagesActivity.class);
                intent.putExtra(ListMessagesFragment.ARG_ITEM, item);

                getActivity().startActivity(intent);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showProgress(final boolean show) {
        if(show){
            mProgressView.setVisibility(View.VISIBLE);
            mProgressView.bringToFront();
        }else{
            mProgressView.setVisibility(View.GONE);
        }
    }

    public class ContactSrvTask extends AsyncTask<Void, Void, ArrayList<UsersModel>> {

        @Override
        protected ArrayList<UsersModel> doInBackground(Void... params) {
            return ((Tower) getActivity().getApplicationContext()).getOnlineUsersMessagesWithLoggedInUser();
        }

        @Override
        protected void onPostExecute(final ArrayList<UsersModel> users) {
            showProgress(false);
            initList(users);
        }
    }
}
