package com.example.jm.clockin.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;


public class MessageModel implements Serializable, Parcelable {
    private static final long serialVersionUID = 1L;

    private String timestamp, message;
    private int id, sender, receiver;

    public MessageModel(int id, int sender, int receiver, String timestamp, String message){
        this.id = id;
        this.sender = sender;
        this.receiver = receiver;
        this.timestamp = timestamp;
        this.message = message;
    }

    protected MessageModel(Parcel in) {
        id = in.readInt();
        sender = in.readInt();
        receiver = in.readInt();
        timestamp = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(sender);
        dest.writeInt(receiver);
        dest.writeString(timestamp);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MessageModel> CREATOR = new Creator<MessageModel>() {
        @Override
        public MessageModel createFromParcel(Parcel in) {
            return new MessageModel(in);
        }

        @Override
        public MessageModel[] newArray(int size) {
            return new MessageModel[size];
        }
    };

    public int getId(){return id;}

    public int getSender(){return sender;}

    public int getReceiver(){return receiver;}

    public String getTimestamp(){return timestamp;}

    public String getMessage(){return message;}

}
