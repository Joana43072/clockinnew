package com.example.jm.clockin.Fragments;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.jm.clockin.Client.Tower;
import com.example.jm.clockin.Dialogs.ErrorDialog;
import com.example.jm.clockin.Models.RecordModel;
import com.example.jm.clockin.R;
import com.example.jm.clockin.Adapter.RecordsAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HistoryFragment extends Fragment {

    private View view;
    private EditText to, from;
    private ArrayList<RecordModel> list;
    private ListView lv;
    private RecordsAdapter l;
    private ProgressBar mProgressView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history, container, false);

        mProgressView = (ProgressBar) view.findViewById(R.id.history_progress);
        mProgressView.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String today = df.format(c.getTime());
        String[] todayArray = today.split(" ");
        final String today_date = todayArray[0];

        list = new ArrayList<>();

        showProgress(true);
        (new HistoryFragment.ContactSrvTask(today_date+" 00:00", today_date+" 23:59")).execute();

        lv = (ListView) view.findViewById(R.id.list_records);
        l = new RecordsAdapter(getContext(), list);
        lv.setAdapter(l);

        Button save = (Button) view.findViewById(R.id.history_btn_save);

        from = (EditText)view.findViewById(R.id.history_date_picker_from);
        to = (EditText)view.findViewById(R.id.history_date_picker_to);
        from.setText(today_date);
        to.setText(today_date);

        from.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setDialog(getResources().getString(R.string.history_range_date_from), from);
            }
        });

        to.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setDialog(getResources().getString(R.string.history_range_date_to), to);
            }
        });

        save.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                boolean error = false;

                String date_from = from.getText().toString();
                String date_to = to.getText().toString();

                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                try {
                    Date date1 = format.parse(date_from);
                    Date date2 = format.parse(date_to);

                    if (date2.compareTo(date1) < 0) {
                        error = true;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if(error){
                    ErrorDialog err_dialog = new ErrorDialog();
                    Bundle args = new Bundle();
                    args.putString("text", getResources().getString(R.string.history_error_invalid_dates));
                    err_dialog.setArguments(args);
                    err_dialog.show(getFragmentManager(), "ErrorDialog");
                }else{
                    showProgress(true);
                    (new HistoryFragment.ContactSrvTask(from.getText().toString()+" 00:00", to.getText().toString()+" 23:59")).execute();
                }
            }
        });



        return view;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showProgress(final boolean show) {
        if(show){
            mProgressView.setVisibility(View.VISIBLE);
            mProgressView.bringToFront();
        }else{
            mProgressView.setVisibility(View.GONE);
        }
    }

    private void setDialog(String title, final EditText picker){
        String text = picker.getText().toString();
        String[] t = text.split("-");
        int day = Integer.parseInt(t[0]);
        int month = Integer.parseInt(t[1]);
        int year = Integer.parseInt(t[2]);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String day = dayOfMonth+"";
                String m = (month+1)+"";

                if(dayOfMonth<10)
                    day = "0"+dayOfMonth;

                if((month+1)<10)
                    m = "0"+(month+1);

                picker.setText(day+"-"+m+"-"+year);
            }
        }, year, (month-1), day);
        mDatePicker.setTitle(title);
        mDatePicker.show();
    }


    private void updateList(final ArrayList<RecordModel> records) {
        if(records==null){
            showProgress(false);
            this.setError();
            return;
        }


        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                list = records;
                l = new RecordsAdapter(getContext(), list);
                synchronized(lv){
                    lv.setAdapter(l);
                    showProgress(false);
                }
            }
        });
    }

    private void setError(){
        ErrorDialog err_dialog = new ErrorDialog();
        Bundle args = new Bundle();
        args.putString("text", "Verifique a sua ligação à rede!");
        err_dialog.setArguments(args);
        err_dialog.show(getFragmentManager(), "ErrorDialog");
    }

    public class ContactSrvTask extends AsyncTask<Void, Void, ArrayList<RecordModel>> {

        private final String begin;
        private final String end;

        ContactSrvTask(String begin, String end) {
            this.begin = begin;
            this.end = end;
        }

        @Override
        protected ArrayList<RecordModel> doInBackground(Void... params) {
            return ((Tower) getActivity().getApplicationContext()).getUserRecords(begin, end);
        }

        @Override
        protected void onPostExecute(final ArrayList<RecordModel> records) {
            updateList(records);
        }

    }

}
