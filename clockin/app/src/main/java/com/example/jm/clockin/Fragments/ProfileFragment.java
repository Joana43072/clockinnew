package com.example.jm.clockin.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.net.Uri;

import com.example.jm.clockin.Client.Tower;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;

import java.util.ArrayList;

public class ProfileFragment extends Fragment {

    UsersModel u;
    EditText username, name, phone, email;
    String photo_name;
    SharedPreferences sharedPref;
    ImageView photo, photo_drawer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
        View header= navigationView.getHeaderView(0);
        photo_drawer = (ImageView) header.findViewById(R.id.menu_user_pic);

        username = (EditText)  view.findViewById(R.id.profile_username);
        email = (EditText) view.findViewById(R.id.profile_email);
        name = (EditText)  view.findViewById(R.id.profile_name);
        phone = (EditText)  view.findViewById(R.id.profile_phone_nr);

        u = ((Tower) getActivity().getApplicationContext()).getUserLoggedIn();
        username.setText(u.getUsername());
        email.setText(u.getEmail());
        name.setText(u.getName());
        phone.setText(u.getPhone_nr()+"");

        photo = (ImageView) view.findViewById(R.id.profile_worker_picture);

        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        photo_name = sharedPref.getString(getString(R.string.preference_file_key_user_photo),"");
        if(photo_name.equals("")){
            photo.setImageResource(R.drawable.ic_profile_avatar);
        }else{
            photo.setImageURI(Uri.parse(photo_name));
        }

        photo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                getActivity().startActivityForResult(pickPhoto , 1);
            }
        });

        Button save = (Button) view.findViewById(R.id.profile_button_save);
        save.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                do_save();
            }
        });

        Button refactor = (Button) view.findViewById(R.id.profile_button_refactor);
        refactor.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                do_refactor();
            }
        });
        return view;
    }

    public void setPhotoName(String photo_name){
        this.photo_name = photo_name;
        Uri photo_uri = Uri.parse(photo_name);
        photo.setImageURI(photo_uri);
    }

    private void do_refactor(){
        u = ((Tower) getActivity().getApplicationContext()).getUserLoggedIn();
        name.setText(u.getName());
        phone.setText(u.getPhone_nr()+"");

        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        photo_name = sharedPref.getString(getString(R.string.preference_file_key_user_photo),"");

        if(photo_name.equals("")){
            photo.setImageResource(R.drawable.ic_profile_avatar);
            photo_drawer.setImageResource(R.drawable.ic_profile_avatar);
        }else{
            photo.setImageURI(Uri.parse(photo_name));
            photo_drawer.setImageURI(Uri.parse(photo_name));
        }
    }

    private void do_save(){
        String text_name = name.getText().toString();
        int text_phone = 0;
        if(!phone.getText().toString().equals(""))
            text_phone = Integer.parseInt(phone.getText().toString());

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.preference_file_key_user_photo),photo_name);
        editor.commit();

        photo_drawer.setImageURI(Uri.parse(photo_name));
        (new ProfileFragment.ContactSrvTask(text_name, text_phone)).execute();
    }

    public class ContactSrvTask extends AsyncTask<Void, Void, Boolean> {

        private final String text_name;
        private final int text_phone;

        ContactSrvTask(String text_name, int text_phone) {
            this.text_name = text_name;
            this.text_phone = text_phone;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            ((Tower) getActivity().getApplicationContext()).setUserLoggedInName(text_name);
            ((Tower) getActivity().getApplicationContext()).setUserLoggedInPhone(text_phone);
            return true;
        }

    }
}
