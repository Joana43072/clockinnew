package com.example.jm.clockin.Client;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

public class ContactArduino {


    public ContactArduino(){ }

    public boolean addMeeting(String office){
        String url = "http://192.168.4.1/meeting";
        try {
            JSONObject message = new JSONObject();
            message.put("office",office);
            executePost(url, message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean executePost(String url, JSONObject jsonObject) {
        System.err.println(">>>>>>>>>> ARDUINO POST: "+url);
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 60000);

        try {

            HttpPost post = new HttpPost();
            URI website = new URI(url);
            post.setURI(website);

            StringEntity se = new StringEntity( jsonObject.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);

            client.execute(post);

            return true;
        }catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

}
