package com.example.jm.clockin.Fragments;

import android.annotation.TargetApi;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.jm.clockin.Client.Tower;
import com.example.jm.clockin.Dialogs.ErrorDialog;
import com.example.jm.clockin.Models.RecordModel;
import com.example.jm.clockin.R;
import com.example.jm.clockin.Adapter.RecordsAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ManualClockInFragment extends Fragment {

    private ArrayList<RecordModel> records_list;
    private ListView lv;
    private RecordsAdapter adapter;
    private ProgressBar mProgressView;

    private static final int ENTRADA = 1;
    private static final int SAIDA = 0;
    private static final int CONFIRMED = 1;
    private static final int NOT_CONFIRMED = 0;
    private static final int SUCESSO = 1;
    private static final int INVALID_TIME = 2;
    private static final int ALREADY_CLOCK_IN = 3;
    private static final int NULL_USER = 4;
    private static final int NOT_CONNECTED = 6;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manual_clock_in, container, false);
        final ToggleButton toggle_type = (ToggleButton) view.findViewById(R.id.manual_clock_in_toggle);

        mProgressView = (ProgressBar) view.findViewById(R.id.manual_clock_in_progress);
        mProgressView.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), android.graphics.PorterDuff.Mode.MULTIPLY);

        records_list = new ArrayList<>();
        lv = (ListView) view.findViewById(R.id.current_records_list);
        adapter = new RecordsAdapter(getContext(), records_list);
        lv.setAdapter(adapter);

        showProgress(true);
        (new ContactSrvGetClockInsTask()).execute();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String today = df.format(c.getTime());
        String[] todayArray = today.split(" ");
        String today_time = todayArray[1];

        final EditText picker = (EditText) view.findViewById(R.id.clock_in_date_picker);
        picker.setText(today_time);
        picker.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int hour, minute;
                String text = picker.getText().toString();
                if(text.equals("")){
                    Calendar mcurrentTime = Calendar.getInstance();
                    hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    minute = mcurrentTime.get(Calendar.MINUTE);
                }else{
                    String[] f_t = text.split(":");
                    hour = Integer.parseInt(f_t[0]);
                    minute = Integer.parseInt(f_t[1]);
                }

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String hour = selectedHour+"";
                        if(selectedHour<10)
                            hour = "0"+selectedHour;
                        picker.setText( hour + ":" + selectedMinute);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle(getResources().getString(R.string.clock_in_select_time));
                mTimePicker.show();
            }
        });

        Button btn_clock_in = (Button) view.findViewById(R.id.button_send_register);
        btn_clock_in.setOnClickListener(new View.OnClickListener()
        {   @Override
            public void onClick(View v){
                //1->ENTRADA
                //0->SAIDA
                int sType = SAIDA;
                if(!toggle_type.isChecked()){
                    sType=ENTRADA;
                }
                String sTime = picker.getText().toString();

                int confirmed = CONFIRMED;

                //se for entrada só se confirma com o arduino
                //se for saida, fica logo confirmado
                if(sType==ENTRADA){
                    confirmed=NOT_CONFIRMED;
                }

                //TODO check for errors
                boolean error = false;
                if(error){
                    ErrorDialog err_dialog = new ErrorDialog();
                    err_dialog.show(getFragmentManager(), "ErrorDialog");
                }else{
                    showProgress(true);
                    (new ManualClockInFragment.ContactSrvAddClockInTask(sType, confirmed, sTime)).execute();
                }
            }
        });

        return view;
    }

    private void updateList(final ArrayList<RecordModel> records) {
        if(records==null){
            showProgress(false);
            this.setError();
            return;
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                records_list = records;
                adapter = new RecordsAdapter(getContext(), records_list);
                synchronized(lv){
                    lv.setAdapter(adapter);
                    showProgress(false);
                }
            }
        });
    }

    private void setError() {
        ErrorDialog err_dialog = new ErrorDialog();
        Bundle args = new Bundle();
        args.putString("text", "Verifique a sua ligação à rede!");
        err_dialog.setArguments(args);
        err_dialog.show(getFragmentManager(), "ErrorDialog");
    }


    private void callList() {
        showProgress(true);
        (new ContactSrvGetClockInsTask()).execute();
    }

    public class ContactSrvGetClockInsTask extends AsyncTask<Void, Void, ArrayList<RecordModel>> {

        @Override
        protected ArrayList<RecordModel> doInBackground(Void... params) {
            return ((Tower) getActivity().getApplicationContext()).getTodayRecords();
        }

        @Override
        protected void onPostExecute(final ArrayList<RecordModel> records) {
            updateList(records);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showProgress(final boolean show) {
        if(show){
            mProgressView.setVisibility(View.VISIBLE);
            mProgressView.bringToFront();
        }else{
            mProgressView.setVisibility(View.GONE);
        }
    }

    public class ContactSrvAddClockInTask extends AsyncTask<Void, Void, Integer> {

        private final int type, status;
        private final String timestamp;

        ContactSrvAddClockInTask(int type, int status, String timestamp){
            this.type = type;
            this.status = status;
            this.timestamp = timestamp;
        }

        @Override
        protected Integer doInBackground(Void... params) {
             return ((Tower) getActivity().getApplicationContext()).addNewRecord(type, timestamp, status);
        }

        @Override
        protected void onPostExecute(final Integer b) {
            showProgress(false);
            switch(b){
                case SUCESSO:
                    callList();
                    break;
                case ALREADY_CLOCK_IN:
                    if(getContext()!=null)
                        Toast.makeText(getContext(), "Alredy clocked in!", Toast.LENGTH_LONG).show();
                    break;
                case NULL_USER:
                    if(getContext()!=null)
                        Toast.makeText(getContext(), "Autentification error!", Toast.LENGTH_LONG).show();
                    break;
                case INVALID_TIME:
                    if(getContext()!=null)
                        Toast.makeText(getContext(), "Invalid times!", Toast.LENGTH_LONG).show();
                    break;
                case NOT_CONNECTED:
                    setError();
                    break;
            }
        }
    }

}