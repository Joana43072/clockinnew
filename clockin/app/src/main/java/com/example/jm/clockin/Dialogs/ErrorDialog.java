package com.example.jm.clockin.Dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.jm.clockin.R;

public class ErrorDialog extends android.support.v4.app.DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.error_dialog, null);

        final String text = getArguments().getString("text");
        TextView t = (TextView) v.findViewById(R.id.dialog_error_text);
        t.setText(text);

        Button ok = (Button) v.findViewById(R.id.dialog_error_btn);
        ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View vv) {
                getDialog().dismiss();
            }
        });
        return v;
    }
}
