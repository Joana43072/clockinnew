package com.example.jm.clockin.Dialogs;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.jm.clockin.LoginActivity;
import com.example.jm.clockin.R;
import com.example.jm.clockin.Client.Tower;

public class LogoutDialog extends android.support.v4.app.DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.logout_leave_dialog, null);
        final String type = getArguments().getString("type");
        String title, text;

        if(type.equals("logout")) {
            title=getResources().getString(R.string.dialog_logout_title);
            text=getResources().getString(R.string.dialog_logout_are_you_sure);
        }else{
            title=getResources().getString(R.string.dialog_quit_title);
            text=getResources().getString(R.string.dialog_quit_are_you_sure);
        }

        getDialog().setTitle(title);

        Button yes = (Button) v.findViewById(R.id.button_dialog_logout_yes);
        Button no = (Button) v.findViewById(R.id.button_dialog_logout_no);
        TextView textView = (TextView) v.findViewById(R.id.textView_text_dialog_logout);
        textView.setText(text);

        yes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View vv) {
                switch(type){
                    case "logout":
                        new LogoutTask().execute();
                        startActivity(new Intent(getContext(), LoginActivity.class));
                        getActivity().finish();
                        getDialog().dismiss();
                        break;
                    case "leave":
                        getActivity().finish();
                        getDialog().dismiss();
                        break;
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return v;
    }

    private class LogoutTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            ((Tower) getActivity().getApplicationContext()).logout();
            return null;
        }
    }
}
