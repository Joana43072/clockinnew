package com.example.jm.clockin.Fragments;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.jm.clockin.Dialogs.ErrorDialog;
import com.example.jm.clockin.Models.UsersModel;
import com.example.jm.clockin.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SettingsFragment extends Fragment {

    private EditText picker_ent_from, picker_ent_to, picker_sai_from, picker_sai_to;
    private LinearLayout autom_sets;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        autom_sets = (LinearLayout) view.findViewById(R.id.autom_settings_hide);

        final Switch switch_chat = (Switch) view.findViewById(R.id.settings_switch_chat);
        final Switch switch_reg_aut = (Switch) view.findViewById(R.id.settings_switch_registo_automatico);

        final NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        switch_chat.setChecked(sharedPref.getBoolean(getString(R.string.preference_file_key_chat),true));

        boolean aut_reg = sharedPref.getBoolean(getString(R.string.preference_file_key_reg_aut),true);
        switch_reg_aut.setChecked(aut_reg);
        if(aut_reg){
            autom_sets.setVisibility(View.VISIBLE);
        }else{
            autom_sets.setVisibility(View.GONE);
        }


        switch_chat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean state = switch_chat.isChecked();
                checkChat(state);
                navigationView.getMenu().findItem(R.id.nav_chat).setVisible(state);
            }
        });

        switch_reg_aut.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkRegAut(switch_reg_aut.isChecked());
            }
        });

        picker_ent_from = (EditText) view.findViewById(R.id.settings_btn_reg_ent_from);
        picker_ent_to = (EditText) view.findViewById(R.id.settings_btn_reg_ent_to);
        picker_sai_from = (EditText) view.findViewById(R.id.settings_btn_reg_sai_from);
        picker_sai_to = (EditText) view.findViewById(R.id.settings_btn_reg_sai_to);

        picker_ent_from.setText(sharedPref.getString(getString(R.string.preference_file_key_ent_from),"00:00"));
        picker_ent_to.setText(sharedPref.getString(getString(R.string.preference_file_key_ent_to),"14:59"));
        picker_sai_from.setText(sharedPref.getString(getString(R.string.preference_file_key_sai_from),"15:00"));
        picker_sai_to.setText(sharedPref.getString(getString(R.string.preference_file_key_sai_to),"23:59"));

        picker_ent_from.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setDialogs(picker_ent_from, getResources().getString(R.string.settings_time_start_entry), "ent_from");
            }
        });

        picker_ent_to.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setDialogs(picker_ent_to, getResources().getString(R.string.settings_time_end_entry), "ent_to");
            }
        });

        picker_sai_from.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setDialogs(picker_sai_from, getResources().getString(R.string.settings_time_start_leave), "sai_from");
            }
        });

        picker_sai_to.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setDialogs(picker_sai_to, getResources().getString(R.string.settings_time_end_entry), "sai_to");
            }
        });

        return view;
    }

    private void setDialogs(final EditText picker, String title, final String type){
        int hour, minute;
        String text = picker.getText().toString();
        String[] f_t = text.split(":");
        hour = Integer.parseInt(f_t[0]);
        minute = Integer.parseInt(f_t[1]);

        TimePickerDialog mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String hour = selectedHour+"";
                String minute = selectedMinute+"";

                if(selectedHour<10)
                    hour = "0"+selectedHour;

                if(selectedMinute<10)
                    minute = "0"+selectedMinute;

                String time =  hour + ":" + minute;
                picker.setText(time);
                setRegegistoAutomaticoSettings(time, type);
            }
        }, hour, minute, true);
        mTimePicker.setTitle(title);
        mTimePicker.show();
    }

    private void checkRegAut(boolean check){
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.preference_file_key_reg_aut),check);
        editor.commit();

        if(check){
            autom_sets.setVisibility(View.VISIBLE);
        }else{
            autom_sets.setVisibility(View.GONE);
        }
    }

    private void checkChat(boolean check){
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.preference_file_key_chat),check);
        editor.commit();
    }

    private void setRegegistoAutomaticoSettings(String set, String type){
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        switch (type){
            case "ent_from":
                editor.putString(getString(R.string.preference_file_key_ent_from),set);
                break;
            case "ent_to":
                editor.putString(getString(R.string.preference_file_key_ent_to),set);
                break;
            case "sai_from":
                editor.putString(getString(R.string.preference_file_key_sai_from),set);
                break;
            case "sai_to":
                editor.putString(getString(R.string.preference_file_key_sai_to),set);
                break;
        }
        editor.commit();

    }

}
